<?php //netteCache[01]000410a:2:{s:4:"time";s:21:"0.87118000 1365249194";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:88:"/www/sites/6/site17366/public_html/other/app/AdminModule/templates/Rubriky/default.latte";i:2;i:1365249193;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"6a33aa6 released on 2012-10-01";}}}?><?php

// source file: /www/sites/6/site17366/public_html/other/app/AdminModule/templates/Rubriky/default.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, '77fsfq90i7')
;
// prolog Nette\Latte\Macros\UIMacros
//
// block obsah
//
if (!function_exists($_l->blocks['obsah'][] = '_lbf4c377608b_obsah')) { function _lbf4c377608b_obsah($_l, $_args) { extract($_args)
?><style type="text/css">
input[type=radio] {
   margin-top: 20px;
}
</style>
				<div class="zarovnaniVlevo">
					<a class="tlacitko odsadVlevo" href="<?php echo htmlSpecialChars($_control->link("vytvoreniRubriky")) ?>
">Vytvořit novou rubriku</a>
				</div>
				<div class="oddelovac"></div>
				<h1>Přehled Rubrik</h1>
    
					<table class="tabulka-vypis-uzivatelu">
						<thead><tr><th>Název</th><th>Název v URL</th><th>Autor</th></tr></thead>
						<tbody>
<?php if ($rubriky == false): ?>
							<tr><td colspan="3">Zatím nebyly vytvořeny žádné rubriky</td></tr>
<?php else: ?>
							<?php echo $rubriky ?>

<?php endif ?>
                  </tbody>
						<tfoot><tr><td></td><td></td><td></td></tr></tfoot>
					</table>			
<?php
}}

//
// block modalniOkno
//
if (!function_exists($_l->blocks['modalniOkno'][] = '_lb85f60f6c2b_modalniOkno')) { function _lb85f60f6c2b_modalniOkno($_l, $_args) { extract($_args)
?>    <!-- #customize your modal window here -->
 
    <div id="dialog" class="window">
    <div id="window-hlavicka">Smazání rubriky<a href="#" class="close"><img src="<?php echo htmlSpecialChars($basePath) ?>/images/button-cross1.png" width="32" /></a></div>
       <div id="window-telo">
       <em>Chceteli smazat rubriku, musíte určit co se má provést s jejím obsahem (stránkami, které obsahuje)<br /> 
       <strong>Pozor: Akce smazání stránek, jež rubrika obsahuje je nevratná! </strong>
       </em>
       <p style="margin-top: 15px;">Co chcete dělat s obsahem mazané rubriky?</p>

<?php Nette\Latte\Macros\FormMacros::renderFormBegin($form = $_form = (is_object("smazaniRubrikyForm") ? "smazaniRubrikyForm" : $_control["smazaniRubrikyForm"]), array()) ?>
       <p style="margin-top: 5px;float:left;"><?php echo $_form["akce"]->getControl()->addAttributes(array()) ?></p>
       <p style="margin-top: 50px;float:left;margin-left:15px;"><select<?php echo $_form["rubriky"]->getControl()->attributes() ?>><option value="0" selected="selected" style="width: 150px;">Žádná</option></select></p>
       <?php echo $_form["idRubriky"]->getControl()->addAttributes(array()) ?>

       <p style="margin-top: 20px;clear:both;clear:left;float:left;margin-left:10px;"><?php echo $_form["smazatRubriku"]->getControl()->addAttributes(array()) ?></p>
       <p style="clear:both;"></p>
<?php Nette\Latte\Macros\FormMacros::renderFormEnd($_form) ?>

       
       </div> 
 
    </div>
<?php
}}

//
// block pomocneSkripty
//
if (!function_exists($_l->blocks['pomocneSkripty'][] = '_lbc04be67c61_pomocneSkripty')) { function _lbc04be67c61_pomocneSkripty($_l, $_args) { extract($_args)
?><script>
$(function(){  

   $(".odstranit:not(a[data-odstranitBezModalniOkna=ne])").bind("click", function(event) {
		//event.preventDefault();
		if (confirm('Opravdu si přejete danou položku odstranit?')) {
			return true;
		}
		else {
			return false;
		}
	});
   
   //select all the a tag with name equal to modal
    $('a[name=modal]:not(a[data-odstranitBezModalniOkna=ano])').click(function(e) {
        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = $(this).attr('data-dialog');
        
        idRubriky = $(this).attr("data-idRubriky");
        $('input[name=idRubriky]').val(idRubriky);
        
        $.get(<?php echo Nette\Templating\Helpers::escapeJs($_control->link("vratRubriky!")) ?>, { id: idRubriky }, function(data) {
         $("#window-telo select").append(data.rubrikyNabidka);
      });
        
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
     
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
         
        //transition effect    
        $('#mask').fadeIn(300);   
        $('#mask').fadeTo(300,0.8); 
     
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
               
        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);
     
        //transition effect
        $(id).fadeIn(300);
     
    });
     
    //if close button is clicked
    $('.window .close').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();
        $('#mask, .window').hide();
    });    
     
    //if mask is clicked
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });   

	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseover", function() {
		$(this).find(".hidden-tools span").css("display", "inline");	
	});
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseout", function() {
		$(this).find(".hidden-tools span").css("display", "none");	
	});
	
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseover", function() {
		$(this).find(".hidden-tools span").css("display", "inline");	
	});
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseout", function() {
		$(this).find(".hidden-tools span").css("display", "none");	
	});
});
</script>
<?php
}}

//
// end of blocks
//

// template extending and snippets support

$_l->extends = empty($template->_extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $template->_extended = $_extended = TRUE;


if ($_l->extends) {
	ob_start();

} elseif (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return Nette\Latte\Macros\CoreMacros::includeTemplate($_l->extends, get_defined_vars(), $template)->render(); }
call_user_func(reset($_l->blocks['obsah']), $_l, get_defined_vars()) ; call_user_func(reset($_l->blocks['modalniOkno']), $_l, get_defined_vars()) ; call_user_func(reset($_l->blocks['pomocneSkripty']), $_l, get_defined_vars()) ; 