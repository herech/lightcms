<?php //netteCache[01]000411a:2:{s:4:"time";s:21:"0.96056700 1365188671";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:89:"/www/sites/6/site17366/public_html/other/app/AdminModule/templates/Napoveda/default.latte";i:2;i:1365188110;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"6a33aa6 released on 2012-10-01";}}}?><?php

// source file: /www/sites/6/site17366/public_html/other/app/AdminModule/templates/Napoveda/default.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, 'jvo87xzcem')
;
// prolog Nette\Latte\Macros\UIMacros
//
// block obsah
//
if (!function_exists($_l->blocks['obsah'][] = '_lb888050d9ba_obsah')) { function _lb888050d9ba_obsah($_l, $_args) { extract($_args)
?><h1>Nápověda</h1>
<div class="hlavni-menu-obal" style="text-align:left;margin-left: 50px;line-height: 1.5;margin-right: 50px;">
<h2>
	<span style="color:#ff3300;"><span style="font-size: 18px;"><span style="font-family: arial,helvetica,sans-serif;">Rubriky</span></span></span></h2>
<ul>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V z&aacute;ložce rubriky se nach&aacute;z&iacute; potřebn&eacute; n&aacute;stroje k tvorbě a spr&aacute;vě rubrik.&nbsp;</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Rubriky jsou n&aacute;strojem k dosažen&iacute; hieachie str&aacute;nek na webu. Tedy rubrika Automobily může obsahovat dal&scaron;&iacute; podrubriky (např. Sportovn&iacute; automobily, Luxusn&iacute; automobily) a tak&eacute; str&aacute;nky, kter&eacute; už ale ž&aacute;dn&eacute; dal&scaron;&iacute; str&aacute;nky či rubriky obsahovat nemůžou a celou hiearchii tak zavr&scaron;uj&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Ve v&yacute;choz&iacute;m stavu se rubrika na webu vykresluje tak, že obsahuje jej&iacute; podrubriky a str&aacute;nky. Toto chov&aacute;n&iacute; můžete změnit a to tak, že j&iacute; definujete jej&iacute; vlastn&iacute; obsah &ndash; rubrika pak v podstatě supluje klasickou str&aacute;nku.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Zde v administraci mohou rubriky prohl&iacute;žet uživatel&eacute; s libovolnou rol&iacute;. Jejich tvorba a spr&aacute;va je v&scaron;ak umožněna pouze uživatelům s rol&iacute; &Scaron;&eacute;fredaktor a v&yacute;&scaron;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Plat&iacute; zde princip vy&scaron;&scaron;&iacute; instance &ndash; tedy uživatel&eacute; s vy&scaron;&scaron;&iacute; rol&iacute; mohou upravovat a mazat kromě sv&yacute;ch rubrik, tak&eacute; rubriky uživatelů s niž&scaron;&iacute; rol&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Aby mohli uživatel&eacute; str&aacute;nek přistupovat k rubrik&aacute;m, je potřeba je přidat do menu. Stejně tak při změně hiearchie rubriky, je potřeba ji vymazat z menu a opět ji tam přidat.</span></span></p>
	</li>
</ul>
<h2>
	<span style="color:#ff3300;"><span style="font-size: 18px;"><span style="font-family: arial,helvetica,sans-serif;">Str&aacute;nky</span></span></span></h2>
<ul>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V z&aacute;ložce str&aacute;nky se nach&aacute;z&iacute; potřebn&eacute; n&aacute;stroje k tvorbě a spr&aacute;vě str&aacute;nek.&nbsp;</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Str&aacute;nky představuj&iacute; samotn&eacute; str&aacute;nky na webov&yacute;ch str&aacute;nk&aacute;ch. Str&aacute;nky existuj&iacute; v určit&eacute; hiearchii, mohou tak b&yacute;t přiřazeny libovoln&eacute; rubrice, nebo mohou st&aacute;t volně.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Zde v administraci mohou str&aacute;nky prohl&iacute;žet, tvořit&nbsp; a spravovat uživatel&eacute; s libovolnou rol&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Plat&iacute; zde princip vy&scaron;&scaron;&iacute; instance &ndash; tedy uživatel&eacute; s vy&scaron;&scaron;&iacute; rol&iacute; mohou upravovat a mazat kromě sv&yacute;ch str&aacute;nek, tak&eacute; str&aacute;nky uživatelů s niž&scaron;&iacute; rol&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Aby mohli uživatel&eacute; str&aacute;nek přistupovat k str&aacute;nk&aacute;m, je potřeba je přidat do menu. Stejně tak při změně hiearchie str&aacute;nky, je potřeba ji vymazat z menu a opět ji tam přidat.</span></span></p>
	</li>
</ul>
<h2>
	<span style="color:#ff3300;"><span style="font-size: 18px;"><span style="font-family: arial,helvetica,sans-serif;">Hlavn&iacute; menu</span></span></span></h2>
<ul>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V z&aacute;ložce hlavn&iacute; menu se nach&aacute;z&iacute; potřebn&eacute; n&aacute;stroje ke spr&aacute;vě hlavn&iacute;ho menu.&nbsp;</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Hlavn&iacute; menu, představuje klasick&eacute; menu, jak jej zn&aacute;me na str&aacute;nk&aacute;ch. Jak&eacute; měnu vytvoř&iacute;te zde, takov&eacute; je dostanete na str&aacute;nk&aacute;ch.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Př&iacute;d&aacute;vat a mazat z něj můžete jak str&aacute;nky tak rubriky. Metodou drag &amp; drop můžete měnit pozice jednotliv&yacute;ch položek menu. Tmavě modr&eacute; jsou rubriky a světle modr&eacute; jsou str&aacute;nky.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Zde v administraci mohou hlavn&iacute; menu upravovat uživatel&eacute; s rol&iacute; &Scaron;&eacute;fredaktor a v&yacute;&scaron;.</span></span></p>
	</li>
</ul>
<h2>
	<span style="color:#ff3300;"><span style="font-family: arial,helvetica,sans-serif;"><span style="font-size: 18px;">M&eacute;dia</span></span></span></h2>
<ul>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V z&aacute;ložce m&eacute;dia se nach&aacute;z&iacute; potřebn&eacute; n&aacute;stroje ke spr&aacute;vě m&eacute;di&iacute;.&nbsp;</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">M&eacute;dia představuj&iacute; jeden ze způsobů, jak na str&aacute;nky nahr&aacute;vat obr&aacute;zky.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Zde v administraci mohou str&aacute;nky prohl&iacute;žet, tvořit&nbsp; a spravovat uživatel&eacute; s libovolnou rol&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Plat&iacute; zde princip vy&scaron;&scaron;&iacute; instance &ndash; tedy uživatel&eacute; s vy&scaron;&scaron;&iacute; rol&iacute; mohou upravovat a mazat kromě sv&yacute;ch obr&aacute;zků, tak&eacute; obr&aacute;zky uživatelů s niž&scaron;&iacute; rol&iacute;.</span></span></p>
	</li>
</ul>
<h2>
	<span style="color:#ff3300;"><span style="font-size: 18px;"><span style="font-family: arial,helvetica,sans-serif;">Uživatel&eacute;</span></span></span></h2>
<ul>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V z&aacute;ložce uživatel&eacute; se nach&aacute;z&iacute; potřebn&eacute; n&aacute;stroje ke spr&aacute;vě uživatelů.&nbsp;</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Můžete zde měnit sv&eacute; osobn&iacute; &uacute;daje.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Zde v administraci mohou uživatele vytv&aacute;řet pouze administr&aacute;toři.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">Plat&iacute; zde princip vy&scaron;&scaron;&iacute; instance &ndash; tedy uživatel&eacute; s vy&scaron;&scaron;&iacute; rol&iacute; mohou upravovat a mazat kromě sebe, tak&eacute; uživatele s niž&scaron;&iacute; rol&iacute;.</span></span></p>
	</li>
	<li>
		<p>
			<span style="font-size:12px;"><span style="font-family: arial,helvetica,sans-serif;">V&yacute;j&iacute;mků tvoř&iacute; Hlavn&iacute; Administr&aacute;tor, kter&aacute; s&aacute;m sebe nemůže smazat.</span></span></p>
	</li>
</ul>


</div>
<div style="clear:both;"></div>
<?php
}}

//
// end of blocks
//

// template extending and snippets support

$_l->extends = empty($template->_extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $template->_extended = $_extended = TRUE;


if ($_l->extends) {
	ob_start();

} elseif (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
?>

<?php if ($_l->extends) { ob_end_clean(); return Nette\Latte\Macros\CoreMacros::includeTemplate($_l->extends, get_defined_vars(), $template)->render(); }
call_user_func(reset($_l->blocks['obsah']), $_l, get_defined_vars()) ; 