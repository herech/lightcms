<?php //netteCache[01]000412a:2:{s:4:"time";s:21:"0.95311900 1365431402";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:90:"/www/sites/6/site17366/public_html/other/app/AdminModule/templates/Uzivatele/default.latte";i:2;i:1365431384;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"6a33aa6 released on 2012-10-01";}}}?><?php

// source file: /www/sites/6/site17366/public_html/other/app/AdminModule/templates/Uzivatele/default.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, 'vkvozf0lmm')
;
// prolog Nette\Latte\Macros\UIMacros
//
// block obsah
//
if (!function_exists($_l->blocks['obsah'][] = '_lbd5ad3f9f1f_obsah')) { function _lbd5ad3f9f1f_obsah($_l, $_args) { extract($_args)
?>				<div class="zarovnaniVlevo">
					<?php if ($user->isInRole("Administrátor")): ?><a class="tlacitko odsadVlevo" href="<?php echo htmlSpecialChars($_control->link("vytvoreniNovehoUzivatele")) ?>
">Vytvořit nového uživatele</a> x <?php endif ?><a class="tlacitko" href="<?php echo htmlSpecialChars($_control->link("upraveniProfilu", array($user->id))) ?>
">Upravení vlastního profilu</a>
				</div>
				<div class="oddelovac"></div>

				<h1>Přehled uživatelů</h1>
    
					<table class="tabulka-vypis-uzivatelu">
						<thead><tr><th>Přihlašovací jméno & Email</th><th>Jméno</th><th>Role</th><th>Datum registrace</th></tr></thead>
						<tbody>
<?php $iterations = 0; foreach ($uzivatele as $uzivatel): ?>
							<tr><td><strong><?php if (($user->isInRole("Administrátor") && $uzivatel->role != "Administrátor" && $uzivatel->role != "Hlavní Administrátor") || ($user->isInRole("Hlavní Administrátor")) || $user->id == $uzivatel->id): ?>
<a class="uzivatel" href="<?php echo htmlSpecialChars($_control->link("upraveniProfilu", array($uzivatel->id))) ?>
"><?php echo Nette\Templating\Helpers::escapeHtml($uzivatel->login, ENT_NOQUOTES) ?>
</a><?php else: ?><a href="" class="uzivatel"><?php echo Nette\Templating\Helpers::escapeHtml($uzivatel->login, ENT_NOQUOTES) ?>
</a><?php endif ?></strong><div class="hidden-tools"><?php if (($user->isInRole("Administrátor") && $uzivatel->role != "Administrátor" && $uzivatel->role != "Hlavní Administrátor") || ($user->isInRole("Hlavní Administrátor")) || $user->id == $uzivatel->id): ?>
<span><a class="upravit" href="<?php echo htmlSpecialChars($_control->link("upraveniProfilu", array($uzivatel->id))) ?>
">Upravit</a><?php if ($uzivatel->role != "Hlavní Administrátor"): ?> | <a name="modal" data-odstranitBezModalniOkna="<?php echo htmlSpecialChars($uzivatel->odstranitBezModalniOkna) ?>
" data-idUzivatele="<?php echo htmlSpecialChars($uzivatel->id) ?>" data-dialog="#dialog" class="odstranit" href="<?php echo htmlSpecialChars($_control->link("odstraneniUzivatele", array($uzivatel->id))) ?>
">Odstranit</a><?php endif ?></span><?php endif ?></div></td><td><?php echo Nette\Templating\Helpers::escapeHtml($uzivatel->jmeno, ENT_NOQUOTES) ?>
 <?php echo Nette\Templating\Helpers::escapeHtml($uzivatel->prijmeni, ENT_NOQUOTES) ?>
</td><td><?php echo Nette\Templating\Helpers::escapeHtml($uzivatel->role, ENT_NOQUOTES) ?>
</td><td><?php echo Nette\Templating\Helpers::escapeHtml($template->date($uzivatel->datum_registrace, '%d. %m. %Y'), ENT_NOQUOTES) ?></td></tr>
<?php $iterations++; endforeach ?>
						</tbody>
						<tfoot><tr><td></td><td></td><td></td><td></td></tr></tfoot>
					</table>			

<?php
}}

//
// block modalniOkno
//
if (!function_exists($_l->blocks['modalniOkno'][] = '_lbc977548ed1_modalniOkno')) { function _lbc977548ed1_modalniOkno($_l, $_args) { extract($_args)
?>    <!-- #customize your modal window here data-dialog=\"#dialog\" name=\"modal\" data-odstranitBezModalniOkna=\"ano\"-->
 
    <div id="dialog" class="window">
    <div id="window-hlavicka">Smazání uživatele<a href="#" class="close"><img src="<?php echo htmlSpecialChars($basePath) ?>/images/button-cross1.png" width="32" /></a></div>
       <div id="window-telo">
       <em>Chceteli smazat uživatele, musíte určit co se má provést s obsahem, který daný uživatel vytvořil (stránky, rubriky, obrázky)<br /> 
       Pokud se rozhodnete smazat uživatelův obsah, stránky nepatřící uživateli, ale přiřazené mazaným rubrikám budou zachovány!<br />
       <strong>Pozor: Akce smazání uživatele je nevratná! </strong>
       </em>
       <p style="margin-top: 15px;">Co chcete dělat s obsahem, který vytvořil daný uživatel?</p>
<?php Nette\Latte\Macros\FormMacros::renderFormBegin($form = $_form = (is_object("smazaniUzivateleForm") ? "smazaniUzivateleForm" : $_control["smazaniUzivateleForm"]), array()) ?>
       <p style="margin-top: 5px;float:left;line-height: 2;"><?php echo $_form["akce"]->getControl()->addAttributes(array()) ?></p>
       <p style="margin-top: 35px;float:left;margin-left:15px;"><?php echo $_form["uzivatele"]->getControl()->addAttributes(array()) ?></p>
       <?php echo $_form["idUzivatele"]->getControl()->addAttributes(array()) ?>

       <p style="margin-top: 20px;clear:both;clear:left;float:left;margin-left:10px;"><?php echo $_form["odstranitUzivatele"]->getControl()->addAttributes(array()) ?></p>
       <p style="clear:both;"></p>
<?php Nette\Latte\Macros\FormMacros::renderFormEnd($_form) ?>

       
       </div> 
 
    </div>
<?php
}}

//
// block pomocneSkripty
//
if (!function_exists($_l->blocks['pomocneSkripty'][] = '_lb4370cdedbf_pomocneSkripty')) { function _lb4370cdedbf_pomocneSkripty($_l, $_args) { extract($_args)
?><script>
$(function(){  

   $(".odstranit:not(a[data-odstranitBezModalniOkna=ne])").bind("click", function(event) {
		//event.preventDefault();
		if (confirm('Opravdu si přejete daného uživatele odstranit?')) {
			return true;
		}
		else {
			return false;
		}
	});
   
   //select all the a tag with name equal to modal
    $('a[name=modal]:not(a[data-odstranitBezModalniOkna=ano])').click(function(e) {
        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = $(this).attr('data-dialog');
        
        idUzivatele = $(this).attr("data-idUzivatele");
        $('input[name=idUzivatele]').val(idUzivatele);
        
        
        //Get the screen height and width
        var maskHeight = $(document).height();
        var maskWidth = $(window).width();
     
        //Set height and width to mask to fill up the whole screen
        $('#mask').css({'width':maskWidth,'height':maskHeight});
         
        //transition effect    
        $('#mask').fadeIn(300);   
        $('#mask').fadeTo(300,0.8); 
     
        //Get the window height and width
        var winH = $(window).height();
        var winW = $(window).width();
               
        //Set the popup window to center
        $(id).css('top',  winH/2-$(id).height()/2);
        $(id).css('left', winW/2-$(id).width()/2);
     
        //transition effect
        $(id).fadeIn(300);
     
    });
     
    //if close button is clicked
    $('.window .close').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();
        $('#mask, .window').hide();
    });    
     
    //if mask is clicked
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });   
});
</script>
<script>
$(function(){

	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseover", function() {
		$(this).find(".hidden-tools span").css("display", "inline");	
	});
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseout", function() {
		$(this).find(".hidden-tools span").css("display", "none");	
	});
	
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseover", function() {
		$(this).find(".hidden-tools span").css("display", "inline");	
	});
	$("#telo3 .tabulka-vypis-uzivatelu tr").bind("mouseout", function() {
		$(this).find(".hidden-tools span").css("display", "none");	
	});
});
</script>
<?php
}}

//
// end of blocks
//

// template extending and snippets support

$_l->extends = empty($template->_extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $template->_extended = $_extended = TRUE;


if ($_l->extends) {
	ob_start();

} elseif (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return Nette\Latte\Macros\CoreMacros::includeTemplate($_l->extends, get_defined_vars(), $template)->render(); }
call_user_func(reset($_l->blocks['obsah']), $_l, get_defined_vars()) ; call_user_func(reset($_l->blocks['modalniOkno']), $_l, get_defined_vars()) ; call_user_func(reset($_l->blocks['pomocneSkripty']), $_l, get_defined_vars()) ; 