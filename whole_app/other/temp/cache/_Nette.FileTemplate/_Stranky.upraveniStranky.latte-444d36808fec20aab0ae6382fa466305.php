<?php //netteCache[01]000418a:2:{s:4:"time";s:21:"0.01733200 1365194378";s:9:"callbacks";a:2:{i:0;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:9:"checkFile";}i:1;s:96:"/www/sites/6/site17366/public_html/other/app/AdminModule/templates/Stranky/upraveniStranky.latte";i:2;i:1365188115;}i:1;a:3:{i:0;a:2:{i:0;s:19:"Nette\Caching\Cache";i:1;s:10:"checkConst";}i:1;s:25:"Nette\Framework::REVISION";i:2;s:30:"6a33aa6 released on 2012-10-01";}}}?><?php

// source file: /www/sites/6/site17366/public_html/other/app/AdminModule/templates/Stranky/upraveniStranky.latte

?><?php
// prolog Nette\Latte\Macros\CoreMacros
list($_l, $_g) = Nette\Latte\Macros\CoreMacros::initRuntime($template, 'qe9qi7b5pq')
;
// prolog Nette\Latte\Macros\UIMacros
//
// block obsah
//
if (!function_exists($_l->blocks['obsah'][] = '_lb4c3f16bee8_obsah')) { function _lb4c3f16bee8_obsah($_l, $_args) { extract($_args)
?>				<h1>Upravení stránky</h1>
				<div class="sirka926px vytvoreni-stranky">
<?php Nette\Latte\Macros\FormMacros::renderFormBegin($form = $_form = (is_object("upraveniStrankyForm") ? "upraveniStrankyForm" : $_control["upraveniStrankyForm"]), array()) ;if ($form->hasErrors()): ?>               <ul class="errors">
<?php $iterations = 0; foreach ($form->errors as $error): ?>                  <li><?php echo Nette\Templating\Helpers::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; endforeach ?>
               </ul>
<?php endif ?>
					<table>
						<tr>
						<td class="odd">
<?php if ($_label = $_form["nazev"]->getLabel()) echo $_label->addAttributes(array()) ?>
						</td>
						<td class="even">
						<?php echo $_form["nazev"]->getControl()->addAttributes(array('value' => "{$stranka->nazev}")) ?>

						</td>
						</tr>
                  <tr>
						<td class="odd">
<?php if ($_label = $_form["nazevVUrl"]->getLabel()) echo $_label->addAttributes(array()) ?>
						</td>
						<td class="even">
						<?php echo $_form["nazevVUrl"]->getControl()->addAttributes(array('value' => "{$stranka->nazev_v_url}")) ?>

						</td>
						</tr>
						<tr>
						<td class="odd">
<?php if ($_label = $_form["hiearchie"]->getLabel()) echo $_label->addAttributes(array()) ?>
						</td>
						<td class="even">
                     <select style="width: 200px"<?php echo $_form["hiearchie"]->getControl()->addAttributes(array (
  'style' => NULL,
))->attributes() ?>>
                     <option value="0" selected="selected">Žádná</option>
                     <?php echo $rubriky ?>

                     </select>	
						</td>
						</tr>
					</table>
					<?php echo $_form["obsahStranky"]->getControl()->addAttributes(array()) ?>

               <?php echo $_form["idStranky"]->getControl()->addAttributes(array('value' => "{$idStranky}")) ?>

					<div style="text-align:left;">
						<?php echo $_form["upravitStranku"]->getControl()->addAttributes(array('style' => 'margin-top: 20px;')) ?>

					</div>
<?php Nette\Latte\Macros\FormMacros::renderFormEnd($_form) ?>
				</div>
            
<?php
}}

//
// block pomocneSkripty
//
if (!function_exists($_l->blocks['pomocneSkripty'][] = '_lb21b3e280d1_pomocneSkripty')) { function _lb21b3e280d1_pomocneSkripty($_l, $_args) { extract($_args)
?><script type="text/javascript" src="<?php echo htmlSpecialChars($baseUrl) ?>/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(function(){
   $("textarea").html(""+<?php echo Nette\Templating\Helpers::escapeJs($stranka->obsah) ?>+"");
   $("select").change(function(){
      $("input[type=\"submit\"]").attr("disabled","true");
      $.get(<?php echo Nette\Templating\Helpers::escapeJs($_control->link("nactiUrl!")) ?>, { id: $(this).val() }, function(data) {
         if (data.url == "") {
            $("input[name=\"nazevVUrl\"]").val(make_url($("input[name=\"nazev\"]").val()));
         }
         else {
            $("input[name=\"nazevVUrl\"]").val(data.url + "/" + make_url($("input[name=\"nazev\"]").val()));
         }
         $("input[type=\"submit\"]").removeAttr("disabled");
          
      });
   });

      var nodiac = { 'á': 'a', 'č': 'c', 'ď': 'd', 'é': 'e', 'ě': 'e', 'í': 'i', 'ň': 'n', 'ó': 'o', 'ř': 'r', 'š': 's', 'ť': 't', 'ú': 'u', 'ů': 'u', 'ý': 'y', 'ž': 'z' };
      /** Vytvoření přátelského URL
      * @param string řetězec, ze kterého se má vytvořit URL
      * @return string řetězec obsahující pouze čísla, znaky bez diakritiky, podtržítko a pomlčku
      * @copyright Jakub Vrána, http://php.vrana.cz/
      */
      function make_url(s) {
          s = s.toLowerCase();
          var s2 = '';
          for (var i=0; i < s.length; i++) {
              s2 += (typeof nodiac[s.charAt(i)] != 'undefined' ? nodiac[s.charAt(i)] : s.charAt(i));
          }
          return s2.replace(/[^a-z0-9_]+/g, '-').replace(/^-|-$/g, '');
      }
   
   $("input[name=\"nazev\"]").keyup(function() {
      
     if ($("select").attr("value") != 0) {
            var value = $("input[name=\"nazevVUrl\"]").val();
            var value1 = $("input[name=\"nazev\"]").val();
            value1 = make_url(value1);
            
            value = value.replace(/^(.*)(\/)([^\/]*)$/,"$1$2"); 
            $("input[name=\"nazevVUrl\"]").val(value + value1);
      }
      else {
         var value = $("input[name=\"nazev\"]").val();
         value = make_url(value);
         $("input[name=\"nazevVUrl\"]").val(value);
      }
      if ($(this).val() != "") {
         $("select").removeAttr("disabled");
      }
      else {
         $("select").attr("disabled","true");
      }
   });

});	

	CKEDITOR.replace( 'obsahStranky',
    {
        filebrowserBrowseUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/ckfinder.html",
        filebrowserImageBrowseUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/ckfinder.html?Type=Images",
        filebrowserFlashBrowseUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/ckfinder.html?Type=Flash",
        filebrowserUploadUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",
        filebrowserImageUploadUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images",
        filebrowserFlashUploadUrl : ""+ <?php echo Nette\Templating\Helpers::escapeJs($baseUrl) ?> +"/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"
    });
</script>
<?php
}}

//
// end of blocks
//

// template extending and snippets support

$_l->extends = empty($template->_extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $template->_extended = $_extended = TRUE;


if ($_l->extends) {
	ob_start();

} elseif (!empty($_control->snippetMode)) {
	return Nette\Latte\Macros\UIMacros::renderSnippets($_control, $_l, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return Nette\Latte\Macros\CoreMacros::includeTemplate($_l->extends, get_defined_vars(), $template)->render(); }
call_user_func(reset($_l->blocks['obsah']), $_l, get_defined_vars()) ; call_user_func(reset($_l->blocks['pomocneSkripty']), $_l, get_defined_vars()) ; 