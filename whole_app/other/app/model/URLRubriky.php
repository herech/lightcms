<?php 

namespace AdminModule;

class URLRubriky {
   private $urlRubriky = "";
   private $urlStranky = "";
   private $counter = 0;
   public function VratUrlStranky($databaze, $idRubriky, $nazevTabulky, $nazevUrl, $nazevIdNadrazeneRubriky) {
     
      $dotaz = $databaze->query("SELECT ".$nazevUrl." FROM ".$nazevTabulky." WHERE ".$nazevIdNadrazeneRubriky." = ?", $idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      $this->urlStranky = ($vysledek[$nazevUrl] == null) ? "" : $vysledek[$nazevUrl];
      return $this->urlStranky;
   }
   
   public function VratUrlRubriky($databaze, $idRubriky, $nazevTabulky, $nazevUrl, $nazevIdNadrazeneRubriky) {
     
     /* $dotaz = $databaze->query("SELECT ".$nazevIdNadrazeneRubriky.", ".$nazevUrl." FROM ".$nazevTabulky." WHERE id = ?", $idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      if ($vysledek) {
         $this->counter++;
         $this->urlRubriky = ($this->urlRubriky == "") ? $vysledek[$nazevUrl] : $vysledek[$nazevUrl] ."/". $this->urlRubriky;
         $this->VratUrlRubriky($databaze, $vysledek[$nazevIdNadrazeneRubriky], $nazevTabulky, $nazevUrl, $nazevIdNadrazeneRubriky);
         return $this->counter;
         return $this->urlRubriky;
      }*/
      $dotaz = $databaze->query("SELECT ".$nazevUrl." FROM ".$nazevTabulky." WHERE id = ?", $idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      $this->urlRubriky = ($vysledek[$nazevUrl] == null) ? "" : $vysledek[$nazevUrl];
      return $this->urlRubriky;
   }
   
   public function ZmenUrlRubriky($databaze, $idRubriky, $regVyraz, $cimNahradit) {
      $result = $databaze->query("SELECT id FROM ad_rubriky WHERE id_nadrazene_rubriky = ? ORDER BY id ASC", $idRubriky);

         while ($r = $result->fetch(\PDO::FETCH_ASSOC)) {
            $URLRubriky = $this->VratUrlRubriky($databaze, $r["id"], "ad_rubriky", "nazev_v_url", "id_nadrazene_rubriky");
            $URLRubriky = preg_replace($regVyraz, $cimNahradit,$URLRubriky,1);
            $databaze->exec("UPDATE ad_rubriky SET nazev_v_url = ? WHERE id = ? ", $URLRubriky, $r["id"]);
            $databaze->exec("UPDATE ad_hlavni_menu SET adresa = ? WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $URLRubriky, $r["id"]);
           
            $dotaz = $databaze->query("SELECT id, nazev_v_url FROM ad_stranky WHERE id_nadrazene_rubriky = ?", $r["id"]);
            while ($vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
               $URLStranky = preg_replace($regVyraz, $cimNahradit,$vysledek["nazev_v_url"],1);
               $databaze->exec("UPDATE ad_stranky SET nazev_v_url = ? WHERE id = ? ", $URLStranky, $vysledek["id"]);
               $databaze->exec("UPDATE ad_hlavni_menu SET adresa = ? WHERE id_polozky = ? AND typ_polozky = 'stranka'", $URLStranky, $vysledek["id"]);
            }
            $this->ZmenUrlRubriky($databaze, $r["id"],$regVyraz, $cimNahradit);
         }
         
   }
   
}