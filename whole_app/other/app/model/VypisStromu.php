<?php
namespace AdminModule; 
use Nette\Environment;
use Nette\Application\UI;

class VypisStromu extends \Nette\Application\UI\Control {
   public $retezec = "";
   public $posledniUroven = 0;
   
   public function __construct() {
      $this->retezec = "";
      $this->posledniUroven = 0;
   }
   
   public function VypisProRubriky($obj) {
   /*if ($obj->level != -1) {

      $user = Environment::getUser();
      $this->retezec .= "<tr><td>".str_repeat("—",($obj->level))."<strong>";

      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $obj->poleSKonkretnimiUdaji["id_autora"])) {
         $odkaz = $this->getPresenter()->link('Rubriky:upraveniRubriky', $obj->poleSKonkretnimiUdaji["id"]);
         $this->retezec .= "<a href=\"$odkaz\" class=\"uzivatel\">".$obj->poleSKonkretnimiUdaji["nazev"]."</a>";
      }
      else {
         $this->retezec .= "<a href=\"\" class=\"uzivatel\">".$obj->poleSKonkretnimiUdaji["nazev"]."</a>";
      }
      $this->retezec .= "</strong><div class=\"hidden-tools\"><span>";
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $obj->poleSKonkretnimiUdaji["id_autora"])) {
         $odkaz = $this->getPresenter()->link('Rubriky:upraveniRubriky', $obj->poleSKonkretnimiUdaji["id"]);
         $this->retezec .= "<a href=\"$odkaz\" class=\"upravit\">Upravit</a> | <a href=\"#\" class=\"odstranit\">Odstranit</a>";
      }
      //error_reporting (E_ALL ^ E_NOTICE);
      $this->retezec .= "</span></div></td><td>". $obj->poleSKonkretnimiUdaji['nazev_v_url'] ."</td></tr>";
      //error_reporting (E_ALL);

      foreach($obj->children as $objekt) {
         $this->retezec .= $this->VypisProRubriky($objekt);
         break;

      }
      return $this->retezec;
   }
       foreach($obj->children as $objekt) {
         $this->retezec .= $this->VypisProRubriky($objekt);
         return $this->retezec;

      }*/
      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProRubriky($objekt);
         }
         return $this->retezec;
      }
      
      $user = Environment::getUser();
      $this->retezec .= "<tr><td>".str_repeat("—",($obj->level))."<strong>";

      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $obj->poleSKonkretnimiUdaji["id_autora"])) {
         $odkaz = $this->getPresenter()->link('Rubriky:upraveniRubriky', $obj->poleSKonkretnimiUdaji["id"]);
         $this->retezec .= "<a href=\"$odkaz\" class=\"uzivatel\">".$obj->poleSKonkretnimiUdaji["nazev"]."</a>";
      }
      else {
         $this->retezec .= "<a href=\"\" class=\"uzivatel\">".$obj->poleSKonkretnimiUdaji["nazev"]."</a>";
      }
      $this->retezec .= "</strong><div class=\"hidden-tools\"><span>";
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $obj->poleSKonkretnimiUdaji["role_autora"])) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $obj->poleSKonkretnimiUdaji["id_autora"])) {
         $odkaz = $this->getPresenter()->link('Rubriky:upraveniRubriky', $obj->poleSKonkretnimiUdaji["id"]);
         if ($obj->poleSKonkretnimiUdaji['obsahujeStranky']) {
            $this->retezec .= "<a href=\"$odkaz\" class=\"upravit\">Upravit</a> | <a href=\"". $this->getPresenter()->link('Rubriky:odstraneniRubriky', $obj->poleSKonkretnimiUdaji["id"]) ."\" data-dialog=\"#dialog\" name=\"modal\" data-odstranitBezModalniOkna=\"ne\" data-idRubriky=\"".$obj->poleSKonkretnimiUdaji["id"]."\" class=\"odstranit\">Odstranit</a>";
         }
         else {
            $this->retezec .= "<a href=\"$odkaz\" class=\"upravit\">Upravit</a> | <a href=\"". $this->getPresenter()->link('Rubriky:odstraneniRubriky', $obj->poleSKonkretnimiUdaji["id"]) ."\" data-dialog=\"#dialog\" name=\"modal\" data-odstranitBezModalniOkna=\"ano\" data-idRubriky=\"".$obj->poleSKonkretnimiUdaji["id"]."\" class=\"odstranit\">Odstranit</a>";
         }
      }
      //error_reporting (E_ALL ^ E_NOTICE);
      $this->retezec .= "</span></div></td><td>". $obj->poleSKonkretnimiUdaji['nazev_v_url'] ."</td><td>". $obj->poleSKonkretnimiUdaji['jmenoAutora'] ."</td></tr>";
      //error_reporting (E_ALL);
      foreach($obj->children as $objekt) {
         $this->VypisProRubriky($objekt);
      }
  
}
   public function VypisProVytvoreniRubriky($obj) {
      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProVytvoreniRubriky($objekt);
         }
         return $this->retezec;
      }

      if ($obj->level == 0) { 
         $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" style=\"font-weight:bold;\">".$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
      }
      else {
         $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\">". str_repeat("&nbsp;",$obj->level * 5) .$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
      }
      
      foreach($obj->children as $objekt) {
         $this->VypisProVytvoreniRubriky($objekt);
      }
  
   }
   public function VypisProUpravuRubriky($obj, $idNadrazeneRubriky) {

      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProUpravuRubriky($objekt, $idNadrazeneRubriky);
         }
         return $this->retezec;
      }

      if ($idNadrazeneRubriky == $obj->poleSKonkretnimiUdaji["id"]) {
         if ($obj->level == 0) { 
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" style=\"font-weight:bold;\" selected=\"selected\">".$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
         else {
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" selected=\"selected\">". str_repeat("&nbsp;",$obj->level * 5) .$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
      }
      else {
         if ($obj->level == 0) { 
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" style=\"font-weight:bold;\">".$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
         else {
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" >". str_repeat("&nbsp;",$obj->level * 5) .$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
      }
      
      foreach($obj->children as $objekt) {
         $this->VypisProUpravuRubriky($objekt, $idNadrazeneRubriky);
      }
   }
   
   public function VypisProOdstraneniRubriky($obj, $idRubriky) {

      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProOdstraneniRubriky($objekt, $idRubriky);
         }
         return $this->retezec;
      }

      if ($idRubriky != $obj->poleSKonkretnimiUdaji["id"]) {
         if ($obj->level == 0) { 
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" style=\"font-weight:bold;padding-left:". $obj->level * 15 ."px;\">".$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
         else {
            $this->retezec .= "<option value=\"".$obj->poleSKonkretnimiUdaji["id"]."\" style=\"padding-left:". $obj->level * 20 ."px;\">". str_repeat("&nbsp;",$obj->level * 5) .$obj->poleSKonkretnimiUdaji["nazev"]."</option>";
         }
      }
      
      foreach($obj->children as $objekt) {
         $this->VypisProOdstraneniRubriky($objekt, $idRubriky);
      }
   }
   
   public function VypisProMenu($obj) {
      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProMenu($objekt);
         }
         $this->retezec .= "</li>";
         return $this->retezec;
      }

      if ($this->posledniUroven > $obj->level) {
         $this->retezec .= str_repeat("</ul></li>", $this->posledniUroven - $obj->level);
         /*$this->retezec .= str_repeat("</li>", $this->posledniUroven - $obj->level);*/
         /*$this->retezec .= "</ul>"; */
      }
      else if ($this->posledniUroven < $obj->level) {
         $this->retezec .= "<ul>"; 
      }
      else {
         $this->retezec .= "</li>";
      }
      
      if ($obj->poleSKonkretnimiUdaji["typ_polozky"] == "rubrika") {
//<img class=\"light\" src=\"". $this->getPresenter()->context->httpRequest->url->basePath ."images/delete-from-menu-light.png\">
         $this->retezec .= "<li data-id=\"".$obj->poleSKonkretnimiUdaji["id_polozky"]."\" class=\"".$obj->poleSKonkretnimiUdaji["typ_polozky"]."\" data-nazev=\"".$obj->poleSKonkretnimiUdaji["nazev"]."\" data-adresa=\"".$obj->poleSKonkretnimiUdaji["adresa"]."\" data-id-nadrazene-rubriky=\"".$obj->poleSKonkretnimiUdaji["id_nadrazene_polozky"]."\"><span class=\"\"></span>".$obj->poleSKonkretnimiUdaji["nazev"] . "<a href=\"#\" class=\"odstranitPolozkuZMenu\">Odstranit</a>";
      }
      else {
         $this->retezec .= "<li data-id=\"".$obj->poleSKonkretnimiUdaji["id_polozky"]."\" class=\"".$obj->poleSKonkretnimiUdaji["typ_polozky"]."\" data-nazev=\"".$obj->poleSKonkretnimiUdaji["nazev"]."\" data-adresa=\"".$obj->poleSKonkretnimiUdaji["adresa"]."\" data-id-nadrazene-rubriky=\"".$obj->poleSKonkretnimiUdaji["id_nadrazene_polozky"]."\"><span class=\"\"></span>".$obj->poleSKonkretnimiUdaji["nazev"] . "<a href=\"#\" class=\"odstranitPolozkuZMenu\">Odstranit</a>";         
      }
      
      $this->posledniUroven = $obj->level;

      foreach($obj->children as $objekt) {
         $this->VypisProMenu($objekt);
      }
   }
   
   public function VypisProPridaniRubrikDoMenu($obj) {
      if ($obj->level == -1) { 
         foreach($obj->children as $objekt) {
            $this->VypisProPridaniRubrikDoMenu($objekt);
         }
         return $this->retezec;
      }

      if ($obj->level == 0) { 
         $this->retezec .= "<input type=\"checkbox\" data-adresa=\"". $obj->poleSKonkretnimiUdaji["nazev_v_url"] ."\" data-nazev=\"".$obj->poleSKonkretnimiUdaji["nazev"]."\" data-id_nadrazene_rubriky=\"".$obj->poleSKonkretnimiUdaji["id_nadrazene_rubriky"]."\" name=\"seznamRubrik\" value=\"".$obj->poleSKonkretnimiUdaji["id"]."\"> <label>".$obj->poleSKonkretnimiUdaji["nazev"]."</label>";
         $this->retezec .= "<br />";
      }
      else {
         $this->retezec .= str_repeat("&nbsp;",$obj->level * 5) . "<input type=\"checkbox\" data-adresa=\"". $obj->poleSKonkretnimiUdaji["nazev_v_url"] ."\" data-nazev=\"".$obj->poleSKonkretnimiUdaji["nazev"]."\" data-id_nadrazene_rubriky=\"".$obj->poleSKonkretnimiUdaji["id_nadrazene_rubriky"]."\" name=\"seznamRubrik\" value=\"".$obj->poleSKonkretnimiUdaji["id"]."\"> <label>".$obj->poleSKonkretnimiUdaji["nazev"]."</label>";
         $this->retezec .= "<br />";
      }
      
      foreach($obj->children as $objekt) {
         $this->VypisProPridaniRubrikDoMenu($objekt);
      }
  
   }
}