<?php
namespace AdminModule; 
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Http\Url;

class UzivatelePresenter extends BasePresenter
{
	protected $uzivatel;
	protected $idUzivatele;

	public function actionOdstraneniUzivatele($idUzivatele) {
			$databaze = new Databaze();
			if ((($this->getUser()->isInRole('Hlavní Administrátor')) && (!in_array('Hlavní Administrátor', $databaze->VratRoleUzivatele($idUzivatele)))) || ($this->getUser()->id == $idUzivatele && !in_array('Hlavní Administrátor', $databaze->VratRoleUzivatele($idUzivatele))) || ($this->getUser()->isInRole('Administrátor') && $databaze->VratRoliUzivatele($idUzivatele) != "Administrátor")) {
				if ($this->getUser()->id == $idUzivatele) {
					$this->getUser()->logout();
				}
				if ($databaze->SmazUzivatele($idUzivatele,WWW_DIR)) {
                $this->flashMessage('Uživatel byl odstraněn', 'success');
            }
            else {
                $this->flashMessage('Uživatel nemohl být odstraněn. Pravděpodobně již byl odstraněn dříve', 'error');
            }
			}
			$this->redirect('Uzivatele:');
			//$this->setView('default');
	}
	
	public function renderDefault()
	{	
		$databaze = new Databaze();
		$uzivatele = $databaze->VratUzivatelskeUdaje();
		$this->template->uzivatele = $uzivatele;
	}
	public function renderUpraveniProfilu($idUzivatele) {
		$user = $this->getUser();
		if($user->id == $idUzivatele) {
			$databaze = new Databaze();
			$uzivatel = $databaze->VratUdajeUzivatele($idUzivatele);
			$this->template->uzivatel = $uzivatel;
			$this->template->idUzivatele = $idUzivatele;
         $this->template->skryjHeslo = false;
		}
		else if ($this->getUser()->isInRole('Administrátor')) {
			$uzivatel = new Uzivatel();
			if (($this->getUser()->isInRole('Hlavní Administrátor')) || (($this->getUser()->isInRole('Administrátor') && ($uzivatel->VratRoliUzivatele($idUzivatele) != 'Hlavní Administrátor' && $uzivatel->VratRoliUzivatele($idUzivatele) != 'Administrátor')))) {
				$databaze = new Databaze();
				$uzivatel = $databaze->VratUdajeUzivatele($idUzivatele);
				$this->template->uzivatel = $uzivatel;
				$this->template->idUzivatele = $idUzivatele;
            $this->template->skryjHeslo = true;
			}
		}
		else {
			$this->redirect('Uzivatele:');
		}
	}
	
	protected function createComponentEditProfileForm()
	{
		$form = new Form();
		$form->addProtection();
		$form->addText('email', 'Email*: ')
      ->setRequired("Vyplňte prosím email")
		->setAttribute('type','email');
		$form->addText('jmeno','Jméno*: ')
      ->setRequired("Vyplňte prosím email");
		$form->addText('prijmeni','Příjmení*: ')
      ->setRequired("Vyplňte prosím email");
		$form->addSelect('role','Role*: ');
		$form->addPassword('newPassword', 'Nové heslo: ')
		->setAttribute('autocomplete','off');
		$form->addPassword('oldPassword', 'Staré heslo: ')
		->setAttribute('autocomplete','off')
		->addConditionOn($form['newPassword'], Form::FILLED)
		->addRule(Form::FILLED, 'Zadejte staré heslo!')
		->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků', 5);
		$form->addHidden('idUzivatele');
		$form->addSubmit('upravitProfil', 'Upravit profil');
		$form->onSuccess[] = $this->editProfileFormSubmitted;
		return $form;
	}

	public function editProfileFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$ukonceniPresenteru = false;
		$uzivatel = new Uzivatel();
		$hesloUzivatele = $uzivatel->VratHesloUzivatele($values->idUzivatele);
		$emailUzivatele = $uzivatel->VratEmailUzivatele($values->idUzivatele);
		$roleUzivatele = implode(",",$uzivatel->VratRoleUzivatele($values->idUzivatele));
		$user = $this->getUser();
		
		if($user->id == $values->idUzivatele) {
			if (hash('sha256', $values->oldPassword) == $hesloUzivatele) {
				if ((strlen($values->newPassword) > 4) && preg_match("/^[\w-\.]+@([\w-]+\\.)+[a-zA-Z]{2,4}$/", $values->email)) {
					
					$hesloUzivatele = hash('sha256', $values->newPassword);
					$emailUzivatele = $values->email;
				}
            else if (preg_match("/^[\w-\.]+@([\w-]+\\.)+[a-zA-Z]{2,4}$/", $values->email)) {
               $emailUzivatele = $values->email;
            }
				else {
					$form->addError("Nesprávně zadaný email nebo heslo (musí mít alespoň 5 znaků).");
					$ukonceniPresenteru = true;
				}
			}
			if ($ukonceniPresenteru == false) {
				if ($user->isInRole('Administrátor') && !$user->isInRole('Hlavní Administrátor')) {
					$roleUzivatele = $values->role;
					$pole = array("Administrátor","Šéfredaktor","Redaktor");
					foreach ($pole as $index => $value) {
						if ($roleUzivatele == $value) {
							$hledanyIndex = $index;
							$pole = array_slice($pole, $hledanyIndex);
							$roleUzivatele = implode(",",$pole);
							break;
						}
					}
					
				}
				if ($uzivatel->UpravitProfil($values->idUzivatele, $values->jmeno, $values->prijmeni, $emailUzivatele, $roleUzivatele, $hesloUzivatele)) {
					$this->flashMessage('Profil upraven.', 'success');
					$this->redirect('this');
				}
				else {
					$this->flashMessage('Vyskytla se chyba. Profil nemohl být upraven.', 'error');
					$this->redirect('this');
				}
			}
		}
		else if ($user->isInRole('Administrátor')) {
			if (($this->getUser()->isInRole('Hlavní Administrátor')) || (($this->getUser()->isInRole('Administrátor') && ($uzivatel->VratRoliUzivatele() != 'Hlavní Administrátor' && $uzivatel->VratRoliUzivatele() != 'Administrátor')))) {
				
				//if (hash('sha256', $values->oldPassword) == $hesloUzivatele) {
					if ((strlen($values->newPassword) > 4) && preg_match("/^[\d\w-\.]+@([\d\w-]+\\.)+[a-zA-Z]{2,4}$/", $values->email)) {
						$hesloUzivatele = hash('sha256', $values->newPassword);
						$emailUzivatele = $values->email;
					}
               else if (preg_match("/^[\w-\.]+@([\w-]+\\.)+[a-zA-Z]{2,4}$/", $values->email)) {
                  $emailUzivatele = $values->email;
               }
					else {
						$form->addError("Nesprávně zadaný email nebo heslo (musí mít alespoň 5 znaků).");
						$ukonceniPresenteru = true;
					}
			//	}
				
				if ($ukonceniPresenteru == false) {
				
					if ($user->isInRole('Administrátor') && !$user->isInRole('Hlavní Administrátor')) {
						$roleUzivatele = $_POST['role'];
						$pole = array("Šéfredaktor","Redaktor");
						foreach ($pole as $index => $value) {
							if ($roleUzivatele == $value) {
								$hledanyIndex = $index;
								$pole = array_slice($pole, $hledanyIndex);
								$roleUzivatele = implode(",",$pole);
								break;
							}
						}
					
					}
					else if ($user->isInRole('Hlavní Administrátor')) {
						$roleUzivatele = $_POST['role'];
						$pole = array("Administrátor","Šéfredaktor","Redaktor");
						foreach ($pole as $index => $value) {
							if ($roleUzivatele == $value) {
								$hledanyIndex = $index;
                        $pole = array_slice($pole, $hledanyIndex);
                        $roleUzivatele = implode(",",$pole);
								break;
							}
						}
					}
					
					if ($uzivatel->UpravitProfil($values->idUzivatele, $values->jmeno, $values->prijmeni, $emailUzivatele, $roleUzivatele, $hesloUzivatele)) {
						$this->flashMessage('Profil upraven.', 'success');
						$this->redirect('this');
					}
					else {
						$this->flashMessage('Vyskytla se chyba. Profil nemohl být upraven.', 'error');
						$this->redirect('this');
					}
				}
			}
		}
		else {
			$this->redirect('Uzivatele:');
		}
	}
	
	public function renderVytvoreniNovehoUzivatele() {
		$user = $this->getUser();
		if (!$user->isInRole("Administrátor")) {
			$this->redirect('Uzivatele:');
		}
	}
	
	protected function createComponentCreateNewUserForm()
	{
		$role = array();
		
		if ($this->getUser()->isInRole("Hlavní Administrátor")) {
			$role = array("Administrátor","Šéfredaktor","Redaktor");
		}
		else {
			$role = array("Šéfredaktor","Redaktor");
		}
		
		$form = new Form();
		$form->addProtection();
		$form->addText('email', 'Email*: ')
		->setAttribute('type','email')
		->setRequired("Vyplňte prosím email")
		->addRule(Form::EMAIL, "Zadejte email ve správném formátu");
		$form->addText('jmeno','Jméno*: ')
      ->setRequired("Zadejte vaše jméno");
		$form->addText('prijmeni','Příjmení*: ')
      ->setRequired("Zadejte vaše příjmení");
		$form->addSelect('role','Role*: ')
		->setItems($role, FALSE);
		$form->addPassword('newPassword', 'Heslo*: ')
		->setRequired('Vyplňte prosím heslo')
		->addRule(Form::MIN_LENGTH,'Heslo musí mít více jak 5 znaků',6)
		->setAttribute('autocomplete','off');
		$form->addPassword('newPasswordConfirm', 'Potvrzení hesla*: ')
		->setRequired('Vyplňte prosím heslo pro potvrzení')
		->setAttribute('autocomplete','off')
		->addRule(Form::EQUAL, 'Hesla se neshodují', $form['newPassword']);
		$form->addSubmit('vytvoritUzivatele', 'Vytvořit uživatele');
		$form->onSuccess[] = $this->createNewUserFormSubmitted;
		return $form;
	}

	public function createNewUserFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$ukonceniPresenteru = false;
		$user = $this->getUser();
		
		
		if ($user->isInRole('Administrátor')) {
				
				$databaze = new Databaze();
				if ($databaze->OverExistenciLoginu($values->email)) {
					$form->addError("Daný email/login je již obsazený");
					$ukonceniPresenteru = true;
				}
				
				if ($ukonceniPresenteru == false) {
				
					if ($user->isInRole('Administrátor') && !$user->isInRole('Hlavní Administrátor')) {
						$roleUzivatele = $_POST['role'];
						$pole = array("Šéfredaktor","Redaktor");
						foreach ($pole as $index => $value) {
							if ($roleUzivatele == $value) {
								$hledanyIndex = $index;
								$pole = array_slice($pole, $hledanyIndex);
								$roleUzivatele = implode(",",$pole);
								break;
							}
						}
					
					}
					else if ($user->isInRole('Hlavní Administrátor')) {
						$roleUzivatele = $_POST['role'];
						$pole = array("Administrátor","Šéfredaktor","Redaktor");
						foreach ($pole as $index => $value) {
							if ($roleUzivatele == $value) {
								$hledanyIndex = $index;
								$pole = array_slice($pole, $hledanyIndex);
								$roleUzivatele = implode(",",$pole);
								break;
							}
						}
					}
					
					if ($databaze->VytvorNovehoUzivatele($values->email, $values->jmeno, $values->prijmeni, $values->email, $roleUzivatele, hash('sha256', $values->newPassword))) {
						$this->flashMessage('Nový uživatel vytvořen, na zadanou adresu byl odeslán email s přihlašovacími údaji a odkazem pro vstup do administrace.', 'success');
						
						$httpRequest = $this->context->httpRequest;
						$url = new Url($httpRequest->getUrl());
						$uri = $url->authority;
                  $urlPath = "/";
                  if (preg_match("#^/www/.+$#",$url->path)) {
                     $urlPath = "/www/";
                  }
                  $uri2 = $url->hostUrl . $urlPath;
						$url = substr($uri, 4);
                  
                  /*------------------------*/
                  	$adresa = $this->link("Homepage:default");
                     $adresa = $_SERVER['SERVER_NAME'] . $adresa;
                  /*------------------------*/
                  
						$formatEmailu = "administrace@". $url ."";
						$mail = new Message;
						$mail->setFrom($formatEmailu)
						->addTo($values->email)
						->setSubject('Nová registrace')
						->setBody("Dobrý den,\nbyl/a jste přidán/a jako nový člen administračního týmu serveru $uri.\nVaše přihlašovací údaje:\nLogin: ". $values->email ."\nHeslo:". $values->newPassword ."\nPro vstup do administrace následujte prosím tento odkaz: ". $adresa)
						->send();
						$this->redirect('this');
					}
					else {
						$this->flashMessage('Vyskytla se chyba. Profil nemohl být upraven.', 'error');
						$this->redirect('this');
					}
				}
		}
		else {
			$this->redirect('Uzivatele:');
		}
	}
   
   protected function createComponentSmazaniUzivateleForm()
	{
      $databaze = new Databaze();
      $seznamUzivatelu = $databaze->VratSeznamUzivatelu();
		$form = new Form();
		$form->addProtection();
      $form->addRadioList('akce', 'Akce ', array("smazat" => " Smazat uživatelův obsah","priradit" => " Přiřadit uživatelův obsah jinému uživateli: "));
      $form['akce']->setDefaultValue('smazat');
      $form->addSelect("uzivatele", "Uživatelé: ", $seznamUzivatelu);
      $form->addHidden("idUzivatele");
		$form->addSubmit('odstranitUzivatele', 'Odstranit uživatele');
		$form->onSuccess[] = $this->smazaniUzivateleFormSubmitted;
		return $form;
	}

	public function smazaniUzivateleFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();

      if (($user->id == $values->idUzivatele) || ($user->isInRole("Administrátor"))) {
         //die($values->akce);
         if ($this->getUser()->id == $values->idUzivatele) {
					$this->getUser()->logout();
         }
         if ($values->akce == "smazat") {
            if ($databaze->SmazUzivatele($values->idUzivatele,WWW_DIR)) {

               $this->flashMessage('Uživatel byl smazán', 'success');
            }
            else $this->flashMessage('Vyskytla se chyba. Uživatel nemohl být smazán', 'error');
         }
         else if ($values->akce == "priradit") {
            $databaze->PriraditObsahUzivateleJinemuUzivateli($values->idUzivatele, $_POST['uzivatele']);
            
            if ($databaze->SmazUzivatele($values->idUzivatele)) {
               $this->flashMessage('Uživatel byl smazán a jeho obsah byl přiřazen vybranému uživateli', 'success');
            }
            else $this->flashMessage('Vyskytla se chyba. Uživatel nemohl být smazán', 'error');
            }
		}
		else {
         $this->flashMessage('Nemáte oprávnění mazat rubriky.', 'error');
		}
      $this->redirect('this');
	}

}
