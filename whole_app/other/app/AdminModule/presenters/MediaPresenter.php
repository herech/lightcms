<?php
namespace AdminModule; 
use Nette\Application\UI\Form;
use Nette\Http\Url;
use Nette\Utils\Strings;

class MediaPresenter extends BasePresenter
{

	public function renderDefault()
	{
		$databaze = new Databaze();
		$media = $databaze->VratMedia();
		//die(var_dump($media));
		if ($media == false) {
			$this->template->media = false;
		}
		else {
			$this->template->media = $media;	
		}
	}
	
	public function renderNahraniSouboru() {
		$maxVelikost = min(ini_get('upload_max_filesize'),ini_get('post_max_size'));
		$this->template->maxVelikost = $maxVelikost;
	}
	
	protected function createComponentNahraniSouboruForm()
	{
		$maxVelikost = min(ini_get('upload_max_filesize'),ini_get('post_max_size'));
		$maxVelikost1 = (int) substr($maxVelikost, 0, -1);
		$form = new Form();
		$form->addProtection();
		$form->addUpload('soubor', 'Soubor*: ')
		->setRequired("Musíte zvolit soubor")
		->addRule(Form::IMAGE, 'Soubor musí být JPEG, PNG nebo GIF.')
        ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je '. $maxVelikost, $maxVelikost1 * 1024 * 1024 /* v bytech */);
		$form->addText('nazev', 'Název: ');
		$form->addText('alternativniText','Alternativní text: ');
		$form->addText('titulek','Titulek: ');
		$form->addSubmit('nahrat', 'Nahrát');
		$form->onSuccess[] = $this->nahraniSouboruFormSubmitted;
		return $form;
	}

	public function nahraniSouboruFormSubmitted(Form $form)
	{
      @ini_set('max_execution_time', 0); 
		$values = $form->getValues();
		$uzivatel = new Uzivatel();
		$databaze = new Databaze();
		$user = $this->getUser();
      $nazev = Strings::webalize($values->nazev == "" ? preg_replace('/^(.+)\\.[^.]+$/', '$1',$values->soubor->getName()) : $values->nazev,".");
		if ($values->soubor->isOk() && $values->soubor->isImage()) {
			if ($databaze->ObrazekSDanymNazvemNeexistuje($nazev, substr($values->soubor->getContentType(),6))) {
            //die(substr($values->soubor->getContentType(),6) . preg_replace('/(.+)\\.(.+)/', '$1',$values->soubor->getName()) . $values->soubor->getTemporaryFile());
            $values->soubor->move(WWW_DIR . "/upload/images/" . $nazev .".". substr($values->soubor->getContentType(),6));
            $httpRequest = $this->context->httpRequest;
            $uri = $httpRequest->getUrl();
            $url = new Url($uri);
            $urlPath = "/";
            if (preg_match("#^/www/.+$#",$url->path)) {
               $urlPath = "/www/";
            }

            $url = $url->hostUrl . $urlPath . "upload/images/" . $nazev .".". substr($values->soubor->getContentType(),6);
            $databaze->NahrajMultimedium($url,$nazev,$values->alternativniText,$values->titulek,$user->id,substr($values->soubor->getContentType(),6));
            $this->flashMessage('Obrázek byl úspěšně nahrán.', 'success');
            $this->redirect('this');
         }
         else {
            $form->addError("Obrázek se stejným názvem a typem již existuje");
         }
		}
		else {
			$form->addError("Nahrání souboru se bohůžel nezdařilo. Vyskytla se chyba.");
		}
	}
   public function renderUpraveniMedia($idMedia) {
      $user = $this->getUser();
      $databaze = new Databaze();
      $medium = $databaze->VratUdajeOMediu($idMedia);
      
      if (($user->isInRole("Administrátor") && $medium->role_autora != "Administrátor" && $medium->role_autora != "Hlavní Administrátor") || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $medium->pole_roli_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $medium->pole_roli_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $medium->id_autora)) {
      
         if ($medium != false) {
            $this->template->medium = $databaze->VratUdajeOMediu($idMedia);
         }
         else {
            $this->redirect('Media:');
         }
      }
      else {
         $this->redirect('Media:');
      }
   }
   
   protected function createComponentUpraveniMediaForm()
	{
		$form = new Form();
		$form->addProtection();
		$form->addText('nazev', 'Název: ')
      ->setRequired("Vyplňte název obrázku!");;
		$form->addText('alternativniText','Alternativní text: ');
		$form->addText('titulek','Titulek: ');
      $form->addHidden('idMedia');
		$form->addSubmit('upravit', 'Upravit');
		$form->onSuccess[] = $this->upraveniMediaFormSubmitted;
		return $form;
	}

	public function upraveniMediaFormSubmitted(Form $form)
	{
		$values = $form->getValues();
      $user = $this->getUser();
      $databaze = new Databaze();
      $medium = $databaze->VratUdajeOMediu($values->idMedia);     
      if (($user->isInRole("Administrátor") && $medium->role_autora != "Administrátor" && $medium->role_autora != "Hlavní Administrátor") || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $medium->pole_roli_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $medium->pole_roli_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $medium->id_autora)) {
         if ($databaze->ObrazekSDanymNazvemNeexistuje(Strings::webalize($values->nazev,"."), $values->idMedia)) {
            $httpRequest = $this->context->httpRequest;
            $uri = $httpRequest->getUrl();
            $url = new Url($uri);
            $urlPath = "/";
            if (preg_match("#^/www/.+$#",$url->path)) {
               $urlPath = "/www/";
            }

            $url = $url->hostUrl . $urlPath . "upload/images/" . Strings::webalize($values->nazev,".") .".";
            
            if ($databaze->UpravUdajeOMediu($values->idMedia, Strings::webalize($values->nazev,"."), $values->alternativniText, $values->titulek, WWW_DIR, $url)) {
               $this->flashMessage('Obrázek byl upraven.', 'success');
            }
            else {
               $this->flashMessage('Obrázek nemohl být upraven, protože neexistuje.', 'error');
            }
            $this->redirect('this');
         }
         else {
            $form->addError("Obrázek se stejným názvem a typem již existuje");
         }
      }
      else {
         $this->redirect('this');
      }
	}
   
   public function actionOdstraneniMedia($idMedia) {
      $user = $this->getUser();
      $databaze = new Databaze();
      $medium = $databaze->VratUdajeOMediu($idMedia);

      if (($user->isInRole("Administrátor") && $medium->role_autora != "Administrátor" && $medium->role_autora != "Hlavní Administrátor") || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $medium->pole_roli_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $medium->pole_roli_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $medium->id_autora)) {
         
         $nazevSouboru = $databaze->VratUmisteniSouboru($idMedia);
         $fail = false;
         if (!file_exists(WWW_DIR . "/upload/images/" . $nazevSouboru)) {
            $fail = true;
         }
         else {
            chmod(WWW_DIR . "/upload/images/" . $nazevSouboru, 0777);
            if (unlink(WWW_DIR . "/upload/images/" . $nazevSouboru) == false) {
               $fail = true;
            }
         }
         if ($databaze->odstranMedium($idMedia) && !$fail) {
            $this->flashMessage('Medium odstraněno', 'success');
         }
         else {
            $this->flashMessage('Medium bohůžel nemohlo být odstraněno. Pravděpodobně už bylo odstraněno dříve', 'error');
         }
         $this->redirect('Media:');
      }
      else {
        $this->redirect('Media:');
      }
   }

}
