<?php

/**
 * Homepage presenter.
 */
namespace AdminModule;  
 
class HomepagePresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

}
