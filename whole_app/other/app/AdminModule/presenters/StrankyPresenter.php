<?php
namespace AdminModule; 
use Nette\Application\UI\Form;
use Nette\Application\Responses\JsonResponse;
use Nette\Utils\Strings;

class StrankyPresenter extends BasePresenter
{
   
   public function handleNactiUrl() {
      $databaze = new Databaze();
      $url = $databaze->NactiUrlRubriky($_GET["id"]);
      $this->sendResponse(new JsonResponse(array("url" => $url)));
   }

   public function actionOdstraneniStranky($idStranky) {
      $databaze = new Databaze();
      $user = $this->getUser();   
      $stranka = $databaze->VratDataProUpraveniStranky($idStranky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $stranka->role_autora)) || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $stranka->role_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $stranka->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $stranka->id_autora)) {
         if ($databaze->SmazStranku($idStranky)) {
            $this->flashMessage("Stránka byla smazána","success");
         }
         else {
            $this->flashMessage("Vyskytla se chyba, stránka nemohla být smazána","error");
         }
      }
      else {
         $this->flashMessage("Nemáte oprávnění mazat tuto stránku","error");
      }
      $this->redirect("Stranky:");
   }

	public function renderDefault()
	{
		$databaze = new Databaze();
      $stranky = $databaze->VratStranky();
      $this->template->stranky = $stranky;
	}
   
   protected function createComponentVytvoreniStrankyForm()
	{
		$form = new Form();
		$form->addProtection();
		$form->addText('nazev', 'Název stránky*: ')
		->setRequired("Musíte Vyplnit název rubriky");
		$form->addText('nazevVUrl', 'Název v URL: ');		
      $form->addSelect('hiearchie','Hiearchie*: ');
		$form->addTextArea('obsahStranky','');
		$form->addSubmit('vytvoritStranku', 'Vytvořit stránku');
		$form->onSuccess[] = $this->vytvoreniStrankyFormSubmitted;
		return $form;
	}

	public function vytvoreniStrankyFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();
      $nazevVUrl = ($values->nazevVUrl == "") ? Strings::webalize($values->nazev) : $values->nazevVUrl;
      if ($databaze->jeUrlObsazene($nazevVUrl)) {
         $form->addError("Dané URL je již obsazené, zvolte prosím jiné");
      }
      else {      
         if ($databaze->VytvoreniStranky($values->nazev, $nazevVUrl, $_POST['hiearchie'], $values->obsahStranky, $user->id)) {
            $this->flashMessage('Stránka byla vytvořena', 'success'); 
         } 
         else {
            $this->flashMessage('Vyskytla se chyba, stránka nemohla být vytvořena.', 'error');
         }
         $this->redirect('this');
      }
	}
   
   public function renderVytvoreniNoveStranky() {
      $this['vypisStromu'] = new VypisStromu();
		$databaze = new Databaze();
		$rubriky = $databaze->VratRubriky();
      if ($rubriky == false) {
			$this->template->rubriky = false;
		}
		else {
         $rubriky = $this['vypisStromu']->VypisProVytvoreniRubriky($rubriky);
			$this->template->rubriky = $rubriky ;
		}
   }
   
   public function renderUpraveniStranky($idStranky) {
      $databaze = new Databaze();
      $user = $this->getUser();   
      $stranka = $databaze->VratDataProUpraveniStranky($idStranky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $stranka->role_autora)) || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $stranka->role_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $stranka->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $stranka->id_autora)) {
         $this->template->stranka = $stranka;
         $this->template->idStranky = $idStranky;
         
         //$rubrika = $databaze->VratDataProUpraveniRubriky($stranka->id_nadrazene_rubriky);
         $this['vypisStromu'] = new VypisStromu();
            $rubriky = $databaze->VratRubriky();
            if ($rubriky == false) {
               $this->template->rubriky = false;
            }
            else {
               $rubriky = $this['vypisStromu']->VypisProUpravuRubriky($rubriky, $stranka->id_nadrazene_rubriky);
               $this->template->rubriky = $rubriky ;
            }
      }
      else {
         $this->flashMessage("Nemáte oprávnění upravovat daou stránku","error");
         $this->redirect("Stranky:");
      }
      
   }
   
   protected function createComponentUpraveniStrankyForm()
	{
		$form = new Form();
		$form->addProtection();
		$form->addText('nazev', 'Název stránky*: ')
		->setRequired("Musíte Vyplnit název rubriky");
		$form->addText('nazevVUrl', 'Název v URL: ');		
      $form->addSelect('hiearchie','Hiearchie*: ');
		$form->addTextArea('obsahStranky','');
      $form->addHidden('idStranky', '');	
		$form->addSubmit('upravitStranku', 'Upravit stránku');
		$form->onSuccess[] = $this->upraveniStrankyFormSubmitted;
		return $form;
	}

	public function upraveniStrankyFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();
      $stranka = $databaze->VratDataProUpraveniStranky($values->idStranky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $stranka->role_autora)) || ($user->isInRole("Šéfredaktor") && !in_array("Šéfredaktor", $stranka->role_autora)) || ($user->isInRole("Redaktor") && !in_array("Redaktor", $stranka->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $stranka->id_autora)) {
         $nazevVUrl = ($values->nazevVUrl == "") ? Strings::webalize($values->nazev) : $values->nazevVUrl;
         if ($databaze->jeUrlObsazene($nazevVUrl, $values->idStranky, "stranka")) {
            $form->addError("Dané URL je již obsazené, zvolte prosím jiné");   
         }
         else {
       
            if ($databaze->UpraveniStranky($values->nazev, $nazevVUrl, $_POST['hiearchie'], $values->obsahStranky, $user->id, $values->idStranky)) {
               $this->flashMessage('Stránka byla upravena', 'success'); 
            } 
            else {
               $this->flashMessage('Vyskytla se chyba, stránka nemohla být upravena', 'error');
            }
            $this->redirect('this');
         }
      }
      else {
         $this->flashMessage('Nemáte oprávnění upravovat danou stránku', 'error');
         $this->redirect('this');
      }
	}

}
