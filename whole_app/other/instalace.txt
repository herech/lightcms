************* D�LE�IT� INFORMACE P�ED SAMOTNOU INSTALAC� *************

Instalovat aplikaci by m�l zku�en�j�� u�ivatel, kter� um� pou��vat aplikace pro spr�vu datab�z� jako phpMyAdmin nebo Adminer. 
Tak� je pot�eba um�t pracovat s FTP serverem.

Aplikace m� ur�it� po�adavky na server, kter� vypl�vaj� z pou�it�ho nette frameworku. Zde jsou tyto po�adavky shrnuty:

PHP min. version 5.3
MySQL min. version 5
.htaccess file protection
.htaccess mod_rewrite
Function ini_set()
Function error_reporting()
Function flock()
Register_globals
Zend.ze1_compatibility_mode
Session auto-start
Reflection extension
SPL extension
PCRE extension
ICONV extension
PHP tokenizer
PDO extension
Multibyte String extension
Multibyte String function overloading
Memcache extension
GD extension
Bundled GD extension
Fileinfo extension or mime_content_type()

Pokud v� server tyto po�adavky nespl�uje, nedoporu�uji aplikaci na takov� server instalovat. 
V�t�ina free hosting� tyto po�adavky v sou�asn� dob� [rok 2013] nespl�uje.
Free hosting www.php5.cz je v�jimka, na n�m aplikaci rozjedete. 
Tak� bal��ek WAMP Server tyto po�adavky respektuje. M��ete jej vyzkou�et pokud chcete aplikaci vyzkou�et na lok�ln�m po��ta�i.
V�t�ina placen�ch webhosting� tyto po�adavky tak� spl�uje.

************* SAMOTN� INSTALACE *************

Samotn� instalace je pom�rn� jednoduch�. 

1. V souboru /app/config/config.neon vypl�te p�ihla�ovac� �daje k datab�zi, je� bude aplikace pou��vat.
2. Celou tuto slo�ku ve kter� je tento dokument nahrajte p�es FTP do ko�enov�ho adres��e va�eho webu.
   (Ide�ln�j�� �e�en�, pokud to v� webhosting povoluje, je namapovat ko�enov� adres�� na slo�ku www, kter� le�� v t�to slo�ce. Nen� to ale nezbytn� nutn�.)
3. P�ihla�te se do aplikace phpMyAdmin, kter� b�� na va�em hostingu, nebo do aplikace Adminer (najdete ji ve slo�ce /www/adminer/, mus�te tak do prohl�e�e zadat "www.nazevVasehoWebu.cz/www/adminer", pop�. jen "www.nazevVasehoWebu.cz/adminer" � podle toho jestli jste mapovali ko�enov� adres�� webu do slo�ky www)
4. V aplikaci phpMyAdmin nebo Admineru vykonejte nad va�� zvolenou datab�z� p��kazy ze souboru tabulky.txt (kter� se nach�z� v t�to slo�ce).
5. Zadejte do prohl�e�e bu� "www.nazevVasehoWebu.cz/www/admin.homepage", pop�. jen "www.nazevVasehoWebu.cz/admin.homepage" (podle toho jestli jste mapovali ko�enov� adres�� webu do slo�ky www) a vypl�te registraci.

M�me Hotovo, nyn� se m��ete p�ihl�sit a za��t pou��vat aplikaci. 


************* DOPL�UJ�C� INFORMACE K INSTALACI *************

Kv�li nahr�v�n� obr�zk� bude mo�n� pot�eba u�init slo�ku www\upload\images zapisovatelnou. Tedy nastaven� CHMOD na 0777. To m��ete prov�st p�es va�eho FTP klienta.


************* INFORMACE PRO WEB V�VOJ��E *************

Aplikace je sice nainstalovan�, ale zm�ny, kter� v n� u�ivatel provede se nikde jinde neprojev�. Je pot�eba ji sp�rovat s va�imi str�nkami. To nen� v�c trivi�ln� a �asov� nen�ro�n�.
Nejlep�� je, pokud jsou va�e str�nky tak� vytvo�eny v nette frameworku. Pak pros�m nakop�rujte aplikaci do slo�ky /app. Pokud v� web nefunguje pod nette frameworkem, nen� to na �kodu. Pak bude nejsp�e nejlep�� um�stit aplikaci do t�to slo�ky, kde se nach�z� i tento soubor. 
Aplikace, respektive u�ivatel pou��vaj�c� aplikaci generuje pot�ebn� data, kter� se ukl�daj� do datab�ze.
Va�� jedinou starost�, tak bude vyt�hnout pot�eban� data z datab�ze. M��ete tak obdr�et nap�. hlavn� menu, jednotliv� str�nky a rubriky. 
Nic v�m ale samoz�ejm� nebr�n� z��dit na str�nk�ch vyhled�v�n�, kde budete vyhled�vat v rubrik�ch a str�nk�ch. 
Data tak m��ete pou��vat libovoln�m zp�sobem.
Pokud um�te programovat v nette, m��ete funk�nost aplikace libovoln�m zp�sobem roz���it.

Pot�ebnou strukturu tabulek m��ete zjistit nap�. v phpMyAdmin nebo Admineru. Okomentovanou strukturu tabulek pak naleznete v m� Maturitn� pr�ci Objektov� orientovan� programov�n�.



