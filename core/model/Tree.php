<?php
namespace AdminModule; 

Class Tree
{
  public $poleSKonkretnimiUdaji = array();
  public $level = -1;
  public $children = array();
  function __construct($databaze,$parent_id_nazev, $nazevTabulky,$parent_id,$poleSUdaji,$level = -1,$poleSKonkretnimiUdaji1 = array())
  {
    $this->level = $level;
         $this->poleSKonkretnimiUdaji = $poleSKonkretnimiUdaji1;
         $result = $databaze->query("SELECT ". implode(",",$poleSUdaji) ." FROM ". $nazevTabulky ." WHERE " . $parent_id_nazev . "=? ORDER BY id ASC", $parent_id);
         while ($r = $result->fetch(\PDO::FETCH_ASSOC)) {
            for ($j = 0; $j < count($poleSUdaji);$j++) {
               $poleSKonkretnimiUdaji1[$poleSUdaji[$j]] = $r[$poleSUdaji[$j]];
            }
            if (in_array("id_autora",$poleSUdaji)) {
               $objektDB = new Databaze();
               $poleSKonkretnimiUdaji1["role_autora"] = $objektDB->VratRoleUzivatele($r["id_autora"]);
               $poleSKonkretnimiUdaji1["obsahujeStranky"] = $objektDB->ObsahujeRubrikaStranky($r["id"]);
               $uzivatelskeUdaje = $objektDB->VratUdajeUzivatele($r["id_autora"]);
               if ($uzivatelskeUdaje->jmeno == "" && $uzivatelskeUdaje->prijmeni == "") {
                  $poleSKonkretnimiUdaji1["jmenoAutora"] = $uzivatelskeUdaje->login;  
               }
               else {
                  $poleSKonkretnimiUdaji1["jmenoAutora"] = $uzivatelskeUdaje->jmeno ." ". $uzivatelskeUdaje->prijmeni;
                  
               }
            }

                  $this->children[] = new Tree($databaze,$parent_id_nazev, $nazevTabulky, $r["id"], $poleSUdaji, ($level + 1), $poleSKonkretnimiUdaji1);

         }
  }
  
} 

/*Class TreeMenu
{
  public $poleSKonkretnimiUdaji = array();
  public $level = -1;
  public $children = array();
  function __construct($databaze,$parent_id_nazev, $nazevTabulky,$parent_id,$poleSUdaji,$level = -1,$poleSKonkretnimiUdaji1 = array())
  {
    $this->level = $level;
         $this->poleSKonkretnimiUdaji = $poleSKonkretnimiUdaji1;
         $result = $databaze->query("SELECT ". implode(",",$poleSUdaji) ." FROM ". $nazevTabulky ." WHERE " . $parent_id_nazev . "=? ORDER BY id ASC", $parent_id);
         while ($r = $result->fetch(\PDO::FETCH_ASSOC)) {
            for ($j = 0; $j < count($poleSUdaji);$j++) {
               $poleSKonkretnimiUdaji1[$poleSUdaji[$j]] = $r[$poleSUdaji[$j]];
            }

            $this->children[] = new TreeMenu($databaze,$parent_id_nazev, $nazevTabulky, $r["id_polozky"], $poleSUdaji, ($level + 1), $poleSKonkretnimiUdaji1);

         }
  }
  
} */
