<?php 

use Nette\Utils\Validators;
namespace AdminModule;

class Databaze { 

	protected $databaze;
   
   protected $dsn; 
   protected $user; 
   protected $password;
   
	protected $spojeni = array();

	public function __construct() {
         //die(self::$dsn . self::$user . " " . self::$password);
         $content = Neon::decode(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/other/app/config/config.neon"));
         $this->dsn = $content["common"]["nette"]["database"]["dsn"];
         $this->user = $content["common"]["nette"]["database"]["user"];
         $this->password = $content["common"]["nette"]["database"]["password"];
         try {
				$this->databaze = new \Nette\Database\Connection($this->dsn, $this->user, $this->password, NULL);
			}
			catch(\PDOException $e)
			{
				exit("Bohůžel se nepodařilo vytvořit databázové spojení.");
			}
	}

   public function jeUrlObsazene($nazevVUrl, $idStrankyNeboRubriky = "", $strankaNeboRubrika = "") {
      
      $dotaz = $this->databaze->query("SELECT nazev_v_url, id FROM ad_rubriky WHERE nazev_v_url = ?", $nazevVUrl);
      $dotaz2 = $this->databaze->query("SELECT nazev_v_url, id FROM ad_stranky WHERE nazev_v_url = ?", $nazevVUrl);
      $vysledek1 = $dotaz->fetch(\PDO::FETCH_ASSOC);
      $vysledek2 = $dotaz2->fetch(\PDO::FETCH_ASSOC);
      
      if ($idStrankyNeboRubriky == "" && $strankaNeboRubrika == "") {
         if ($vysledek1["nazev_v_url"] == null && $vysledek2["nazev_v_url"] == null) {
            return false;
         }
         else return true;
      }
      else {
         if ($strankaNeboRubrika == "stranka") {
            if ($vysledek1["nazev_v_url"] == null && ($vysledek2["nazev_v_url"] == null || ($vysledek2["nazev_v_url"] != null && $vysledek2["id"] == $idStrankyNeboRubriky))) {
               return false;
            }
            else return true;
         }
         else if ($strankaNeboRubrika == "rubrika") {
            if ($vysledek2["nazev_v_url"] == null && ($vysledek1["nazev_v_url"] == null || ($vysledek1["nazev_v_url"] != null && $vysledek1["id"] == $idStrankyNeboRubriky))) {
               return false;
            }
            else return true;
         }
      }
   }
   
	/*------------------------------METODY V PROCESU REGISTRACE-------------------------------------*/	

	public function OverJestliUzMameHlavnihoAdmina() {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_uzivatele WHERE role LIKE '%Hlavní Administrátor%'");
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 0)
			return false;
		else return true;
		
	}
	public function RegistrovatUzivatele($login, $password, $email) {
		
		$password = hash('sha256', $password);
		$dotaz = $this->databaze->exec("INSERT INTO ad_uzivatele (login, password, email, role, datum_registrace) VALUES (?, ?, ?, ?, ?)", $login,$password,$email,'Hlavní Administrátor,Administrátor,Šéfredaktor,Redaktor',new \DateTime);
		if ($dotaz != 1)
			throw new DatabaseException("Nepodařilo se zaregistrovat");

	}
/*------------------------------METODY V PROCESU PŘIHLÁŠENÍ-------------------------------------*/		

	public function OverPrihlasovaciUdajeUzivatele($login, $password) {
		
	}
	
	/*public function VratUserId() {
		return $this->userId;
	}*/
	
	/*------------------------------METODY V PROCESU SMAZANI UZIVATELE-------------------------------------*/	
	public function SmazUzivatele($id,$wwwDir) {
      $dotaz = $this->databaze->query("SELECT nazev FROM ad_media WHERE id_autora = ?",$id);
      while ($objektVysledku = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
         chmod($wwwDir . "/upload/images/" . $objektVysledku["nazev"] .".". $objektVysledku["typ"], 0777);
         unlink($wwwDir . "/upload/images/" . $objektVysledku["nazev"] .".". $objektVysledku["typ"]);
      }
      $this->databaze->exec("DELETE FROM ad_media WHERE id_autora = ?",$id);
      $this->databaze->exec("DELETE FROM ad_stranky WHERE id_autora = ?",$id);
      $dotaz = $this->databaze->query("SELECT id FROM ad_rubriky WHERE id_autora = ?",$id);
      while ($objektVysledku = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
         $hodnota = $this->SmazRubriku($objektVysledku["id"], false);
         $this->databaze->exec("UPDATE ad_stranky SET id_nadrazene_rubriky = 0 WHERE id_nadrazene_rubriky = ?",$objektVysledku["id"]);
      }
		if ($this->databaze->exec("DELETE FROM ad_uzivatele WHERE id = ?",$id) == 1) return true;
      else return false;
	}
   
   public function PriraditObsahUzivateleJinemuUzivateli($idUzivatele, $idNovehoUzivatele) {
      $this->databaze->exec("UPDATE ad_media SET id_autora = ? WHERE id_autora = ?",$idNovehoUzivatele,$idUzivatele);
      $this->databaze->exec("UPDATE ad_stranky SET id_autora = ? WHERE id_autora = ?",$idNovehoUzivatele,$idUzivatele);
      $this->databaze->exec("UPDATE ad_rubriky SET id_autora = ? WHERE id_autora = ?",$idNovehoUzivatele,$idUzivatele);
   }
   
	/*------------------------------METODY V PROCESU VRACENI ROLE UZIVATELE-------------------------------------*/	
	
	public function VratRoliUzivatele($id) {
			$dotaz = $this->databaze->query("SELECT role FROM ad_uzivatele WHERE id = ?",$id);
			$objektVysledku = $dotaz->fetch();
			$poleRoli = explode(",", $objektVysledku[0]); 
			return $poleRoli[0];
			
	}
	public function VratRoleUzivatele($id) {
			$dotaz = $this->databaze->query("SELECT role FROM ad_uzivatele WHERE id = ?",$id);
			$objektVysledku = $dotaz->fetch();
			$poleRoli = explode(",",$objektVysledku[0]);
			return $poleRoli;
			
	}
	/*------------------------------METODY V PROCESU VRACENI DAT UZIVATELE-------------------------------------*/	
	
	public function VratUzivatelskeUdaje() {
		
		$dotaz = $this->databaze->query("SELECT id, login, jmeno, prijmeni, email, role, datum_registrace FROM ad_uzivatele");
		
		$poleUzivatelu = array();
		
		while($radek = $dotaz->fetch(\PDO::FETCH_OBJ)) {
			$uzivatel = new \StdClass;
			$uzivatel->id = $radek->id;
			$uzivatel->login = $radek->login;
			$uzivatel->jmeno = $radek->jmeno;
			$uzivatel->prijmeni = $radek->prijmeni;
			$uzivatel->email = $radek->email;
         
         $odstranitBezModalniOkna = "ano";
         $dotaz2 = $this->databaze->query("SELECT COUNT(*) FROM ad_rubriky WHERE id_autora = ?", $uzivatel->id);
         $vysledek2 = $dotaz2->fetch();
         $dotaz3 = $this->databaze->query("SELECT COUNT(*) FROM ad_stranky WHERE id_autora = ?", $uzivatel->id);
         $vysledek3 = $dotaz3->fetch();
         $dotaz4 = $this->databaze->query("SELECT COUNT(*) FROM ad_media WHERE id_autora = ?", $uzivatel->id);
         $vysledek4 = $dotaz4->fetch();

         if (($vysledek2[0] > 0) || ($vysledek3[0] > 0) || ($vysledek4[0] > 0)) {
         $odstranitBezModalniOkna = "ne";
         }
         $uzivatel->odstranitBezModalniOkna = $odstranitBezModalniOkna;
			
         $role = explode(",", $radek->role);
			$uzivatel->role = $role[0];
			$uzivatel->datum_registrace = $radek->datum_registrace;
			$poleUzivatelu[] = $uzivatel;
		}
		return $poleUzivatelu;
	}
	
	public function VratUdajeUzivatele($idUzivatele) {
		$dotaz = $this->databaze->query("SELECT id, login, jmeno, prijmeni, email, role, datum_registrace FROM ad_uzivatele WHERE id = ?", $idUzivatele);
			
			$radek = $dotaz->fetch(\PDO::FETCH_OBJ);
			
			if ($radek) {			
				$uzivatel = new \StdClass;
				$uzivatel->id = $radek->id;
				$uzivatel->login = $radek->login;
				$uzivatel->jmeno = $radek->jmeno;
				$uzivatel->prijmeni = $radek->prijmeni;
				$uzivatel->email = $radek->email;
				$role = explode(",", $radek->role);
				$uzivatel->role = $role[0];
				$uzivatel->datum_registrace = $radek->datum_registrace;
				return $uzivatel;
			}
	
	}
   
   public function VratSeznamUzivatelu() {
   	$dotaz = $this->databaze->query("SELECT id, jmeno, prijmeni FROM ad_uzivatele");
		
		$poleUzivatelu = array();
		
		while($radek = $dotaz->fetch(\PDO::FETCH_OBJ)) {

			$poleUzivatelu[$radek->id] = $radek->jmeno . " " . $radek->prijmeni;
		}
		return $poleUzivatelu;
   }
	
	/*------------------------------METODY V PROCESU NASTAVENÍ NOVÉHO HESLA-------------------------------------*/		

	public function OverExistenciEmailu($email) {
		
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_uzivatele WHERE email = ?", $email);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;

	}
	
	public function NastavToken($email, $token) {
	
	$this->databaze->exec("UPDATE ad_uzivatele SET password_token = ?,password_token_generated = ? WHERE email = ?",$token,new \DateTime,$email);

	}
	
	public function OverPlatnostUdaju($email, $token) {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_uzivatele WHERE email = ? AND password_token = ? AND password_token_generated >= ? - INTERVAL 1 DAY", $email, $token, new \DateTime);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;
		
	}
	
	public function NastavNoveHeslo($password, $email) {
		
		$this->databaze->exec("UPDATE ad_uzivatele SET password_token_generated = NULL, password_token = NULL, password = ? WHERE email = ?", $password, $email);
		
	}
	
	/*------------------------------METODY V PROCESU UPRAVENÍ PROFILU-------------------------------------*/		
	
	
	public function VratHesloUzivatele($idUzivatele) {
		$dotaz = $this->databaze->query("SELECT password FROM ad_uzivatele WHERE id = ?",$idUzivatele);
			$objektVysledku = $dotaz->fetch();
			return $objektVysledku[0];
	}
	
	public function VratEmailUzivatele($idUzivatele) {
		$dotaz = $this->databaze->query("SELECT email FROM ad_uzivatele WHERE id = ?",$idUzivatele);
			$objektVysledku = $dotaz->fetch();
			return $objektVysledku[0];
	}

	public function UpravitProfil($idUzivatele, $jmeno, $prijmeni, $email, $role, $heslo) {
   	//die ($idUzivatele . $jmeno . $prijmeni . $email . $role . $heslo);
		$dotazAliasPocetOvlivnenychSloupcu = $this->databaze->exec("UPDATE ad_uzivatele SET jmeno = ?, prijmeni = ?, login = ?, email = ?, role = ?, password = ? WHERE id = ?",$jmeno, $prijmeni, $email, $email, $role, $heslo, $idUzivatele);
		if ($dotazAliasPocetOvlivnenychSloupcu == 1)	return true;
		else return false;
	}

/*------------------------------METODY V PROCESU VYTVOŘENÍ NOVÉHO UŽIVATELE-------------------------------------*/

public function OverExistenciLoginu($login) {
	$dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_uzivatele WHERE login = ?", $login);
	$objektVysledku = $dotaz->fetch();
	if ($objektVysledku[0] == 1)
		return true;
	else return false;
}

public function VytvorNovehoUzivatele($login, $jmeno, $prijmeni, $email, $role, $heslo) {
	$dotazAliasPocetOvlivnenychSloupcu = $this->databaze->exec("INSERT INTO ad_uzivatele(login,password,jmeno,prijmeni,email,role,datum_registrace) VALUES (?,?,?,?,?,?,?)",$login, $heslo, $jmeno, $prijmeni, $email, $role, new \DateTime);
	if ($dotazAliasPocetOvlivnenychSloupcu == 1)	
		return true;
	else return false;
}		

/*------------------------------METODY V PROCESU NAHRÁNÍ MULTIMÉDIA-------------------------------------*/	


public function NahrajMultimedium($adresa,$nazev,$alternativni_text,$titulek,$id_autora,$typ) {
	$dotazAliasPocetOvlivnenychSloupcu = $this->databaze->exec("INSERT INTO ad_media(adresa,nazev,alternativni_text,titulek,id_autora,typ,datum_nahrani) VALUES (?,?,?,?,?,?,?)",$adresa,$nazev,$alternativni_text,$titulek,$id_autora,$typ, new \DateTime);
	if ($dotazAliasPocetOvlivnenychSloupcu == 1)	
		return true;
	else return false;
}

public function VratMedia() {
			//$this->PriraditObsahUzivateleJinemuUzivateli(1, 2);
			$dotaz = $this->databaze->query("SELECT id FROM ad_media");
			
			$radek = $dotaz->fetch();
			
			if ($radek) {	
			
			$dotaz = $this->databaze->query("SELECT id, adresa,nazev,alternativni_text,titulek,id_autora,typ,datum_nahrani FROM ad_media ORDER BY id DESC");
				
				$poleMedii = array();
				
				while ($radek = $dotaz->fetch(\PDO::FETCH_OBJ)) {
					$media = new \StdClass;
					$media->id = $radek->id;
					$media->adresa = $radek->adresa;
					$media->nazev = $radek->nazev;
					$media->alternativni_text = $radek->alternativni_text;
					$media->titulek = $radek->titulek;
					$media->id_autora = $radek->id_autora;
					$dotaz2 = $this->databaze->query("SELECT login, jmeno, prijmeni FROM ad_uzivatele WHERE id = ?", $radek->id_autora);	
					$radek2 = $dotaz2->fetch(\PDO::FETCH_ASSOC);
               $login_autora = "";
               if ($radek2["jmeno"] == "" && $radek2["prijmeni"] == "") {
                  $login_autora  = $radek2["login"];
               }
               else {
                  $login_autora  = $radek2["jmeno"] ." ". $radek2["prijmeni"];  
               }
					$media->login_autora = $login_autora;	
					$media->role_autora = $this->VratRoliUzivatele($radek->id_autora);
               $media->pole_roli_autora = $this->VratRoleUzivatele($radek->id_autora);
					$media->typ = $radek->typ;
					$media->datum_nahrani = $radek->datum_nahrani;
					$poleMedii[] = $media;
				}
				return $poleMedii;
			}
			else {
				return false;
			}
	
	}
   
   public function odstranMedium($id) {
      if ($this->databaze->exec("DELETE FROM ad_media WHERE id = ?", $id) == 1) return true;
      else return false;
   }
   
   public function VratUdajeOMediu($idMedia) {
      $dotaz = $this->databaze->query("SELECT id, adresa,nazev,alternativni_text,titulek,id_autora,typ,datum_nahrani FROM ad_media WHERE id = ?", $idMedia);
      
      $radek = $dotaz->fetch(\PDO::FETCH_OBJ);
      
      if ($radek) {
         $medium = new \StdClass;
         $medium->id = $radek->id;
         $medium->adresa = $radek->adresa;
         $medium->nazev = $radek->nazev;
         $medium->alternativni_text = $radek->alternativni_text;
         $medium->titulek = $radek->titulek;
         $medium->id_autora = $radek->id_autora;
         $dotaz2 = $this->databaze->query("SELECT login FROM ad_uzivatele WHERE id = ?", $radek->id_autora);	
         $radek2 = $dotaz2->fetch();
         $medium->login_autora = $radek2[0];
         $medium->role_autora = $this->VratRoliUzivatele($radek->id_autora);
         $medium->pole_roli_autora = $this->VratRoleUzivatele($radek->id_autora);         
         $medium->typ = $radek->typ;
         $medium->datum_nahrani = $radek->datum_nahrani;
         
         return $medium;
      }
      else {
         return false;
      }
   }
   
   public function VratUmisteniSouboru($id) {
      $dotaz = $this->databaze->query("SELECT adresa FROM ad_media WHERE id = ?", $id);
      
      $radek = $dotaz->fetch();
      return preg_replace('#(.+)/([^/]+\\.(.+))#', '$2',$radek[0]);
   }
   
   public function UpravUdajeOMediu($id, $nazev, $alternativniText, $titulek,$wwwDir,$URL) {
      $dotaz = $this->databaze->query("SELECT nazev, typ FROM ad_media WHERE id = ?",$id);
      $objektVysledku = $dotaz->fetch(\PDO::FETCH_ASSOC);

   $dotaz0 = $this->databaze->query("SELECT adresa FROM ad_media WHERE id = ?", $id);
      $objVysledku0 = $dotaz0->fetch(\PDO::FETCH_ASSOC);
      $staraAdresa = $objVysledku0["adresa"];
      
      $dotaz1 = $this->databaze->query("SELECT id, obsah FROM ad_stranky WHERE obsah LIKE '%".$staraAdresa."%'");
      $objVysledku1 = $dotaz1->fetch(\PDO::FETCH_ASSOC);
      $obsah = preg_replace("#(.*)(".$staraAdresa.")(.*)#","$1".$URL . $objektVysledku["typ"]."$3",$objVysledku1["obsah"]);
      
      $this->databaze->exec("UPDATE ad_stranky SET obsah = ? WHERE id = ?", $obsah,$objVysledku1["id"]);
      
      //die($wwwDir . "/upload/images/" . $objektVysledku["nazev"] .".".  $objektVysledku["typ"]);
      chmod($wwwDir . "/upload/images/" . $objektVysledku["nazev"] .".". $objektVysledku["typ"], 0777);
      rename($wwwDir . "/upload/images/" . $objektVysledku["nazev"] .".". $objektVysledku["typ"], $wwwDir . "/upload/images/" . $nazev .".". $objektVysledku["typ"]);
      if ($this->databaze->exec("UPDATE ad_media SET nazev = ?, alternativni_text = ?, titulek = ?, adresa = ? WHERE id = ?", $nazev, $alternativniText,  $titulek, $URL . $objektVysledku["typ"],$id) == 1) return true;
      else return false;
   }
   
   public function ObrazekSDanymNazvemNeexistuje($nazev, $typOrId) {
      if (!is_numeric($typOrId)) {
         $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_media WHERE nazev = ? AND typ = ?",$nazev, $typOrId);
         $objektVysledku = $dotaz->fetch();
         if ($objektVysledku[0] > 0) {
            return false;
         }
         else {
            return true;
         }
      }
      else {
         $dotaz = $this->databaze->query("SELECT typ FROM ad_media WHERE id = ?", $typOrId);
         $objektVysledku = $dotaz->fetch(\PDO::FETCH_ASSOC);
         $typ = $objektVysledku["typ"];
         $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_media WHERE nazev = ? AND typ = ?",$nazev, $typ);
         $objektVysledku = $dotaz->fetch();
         if ($objektVysledku[0] > 0) {
            return false;
         }
         else {
            return true;
         }
      }
   }
   /*------------------------------METODY V PROCESU Rubrik-------------------------------------*/		

   public function ExistujiRubriky() {
      $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_rubriky");
      $vysledek = $dotaz->fetch();
      if ($vysledek[0]) {
         return true;
      }
      else return false;
   }
   
   public function VratRubriky() {
      $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_rubriky");
			
			$radek = $dotaz->fetch();
			
			if ($radek[0]) {	
            $poleSUdaji = array("id", "nazev","id_nadrazene_rubriky", "obsah","nazev_v_url","id_autora");
				$tree = new Tree($this->databaze,"id_nadrazene_rubriky", "ad_rubriky",0,$poleSUdaji);
				return $tree;
			}
			else {
				return false;
			}
   }
   
   public function VytvoreniRubriky($nazev, $id_nadrazene_rubriky, $obsah, $nazev_v_url, $id_autora) {
      if ($this->databaze->exec("INSERT INTO ad_rubriky(nazev,id_nadrazene_rubriky,obsah,nazev_v_url,id_autora) VALUES (?,?,?,?,?)", $nazev,$id_nadrazene_rubriky,$obsah,$nazev_v_url,$id_autora) == 1) return true;
      else return false;
   }
   
   public function VratDataProUpraveniRubriky($idRubriky) {
      $dotaz = $this->databaze->query("SELECT nazev,id_nadrazene_rubriky,obsah,nazev_v_url,id_autora FROM ad_rubriky WHERE id = ?",$idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      $rubrika = array();
      $rubrika["nazev"] = $vysledek["nazev"];
      $rubrika["id_nadrazene_rubriky"] = $vysledek["id_nadrazene_rubriky"];
      $rubrika["obsah"] = $vysledek["obsah"];
      $rubrika["nazev_v_url"] = $vysledek["nazev_v_url"];
      $rubrika["id_autora"] = $vysledek["id_autora"];
      $rubrika["role_autora"] = $this->VratRoleUzivatele($vysledek["id_autora"]);
      return $rubrika;
   }
   
   public function UpraveniRubriky($nazev, $id_nadrazene_rubriky, $obsah, $nazev_v_url, $id_autora, $id) {

      $dotaz = $this->databaze->query("SELECT nazev_v_url FROM ad_rubriky WHERE id = ?",$id);
      $nazevVURL = $dotaz->fetch();
      $nazevVURL = $nazevVURL[0];
      
      if ($this->databaze->exec("UPDATE ad_rubriky SET nazev = ?,id_nadrazene_rubriky = ?,obsah = ?,nazev_v_url = ?,id_autora = ? WHERE id = ?", $nazev,$id_nadrazene_rubriky,$obsah,$nazev_v_url,$id_autora, $id) == 1) {
         $this->databaze->exec("UPDATE ad_hlavni_menu SET adresa = ? WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $nazev_v_url, $id);
         $this->databaze->exec("UPDATE ad_hlavni_menu SET nazev = ? WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $nazev, $id);
  
      
      $urlRubriky = new URLRubriky();
      
      $regVyraz = "";
      $cimNahradit = "";
      $regVyraz = "#^(.*)(".$nazevVURL.")(.*)$#";
      $cimNahradit = '${1}'. $nazev_v_url .'$3';

      
      $urlRubriky->ZmenUrlRubriky($this->databaze, $id, $regVyraz, $cimNahradit);
      
         
         return true;
      }
      else return false;
   }
   
   public function DataPristupnostiRubriky($idRubriky) {
      $dotaz = $this->databaze->query("SELECT id_autora FROM ad_rubriky WHERE id = ?",$idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_OBJ);
      $rubrika = new \StdClass;
      $rubrika->id_autora = $vysledek->id_autora;
      $rubrika->role_autora = $this->VratRoleUzivatele($vysledek->id_autora);
      return $rubrika;
   }
   
   public function NactiUrlRubriky($idRubriky) {
      $URLRubriky = new URLRubriky();
      return $URLRubriky->VratUrlRubriky($this->databaze, $idRubriky, "ad_rubriky", "nazev_v_url", "id_nadrazene_rubriky");
   }
   
   public function VratPoleRubrik() {
      $dotaz = $this->databaze->query("SELECT id, nazev FROM ad_rubriky");
      $poleRubrik = array();
      $poleRubrik[0] = "Žádná";
      while ($vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
         $poleRubrik[$vysledek["id"]] = $vysledek["nazev"];
      }
      return $poleRubrik;
   }
   
   public function ObsahujeRubrikaStranky($idRubriky) {
      $dotaz = $this->databaze->query("SELECT nazev FROM ad_stranky WHERE id_nadrazene_rubriky =?", $idRubriky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      if ($vysledek) {
         return true;
      }
      else return false;
   }
   
   public function SmazRubriku($idRubriky, $trueOrFalseOridNoveRubriky) {
      $dotaz = $this->databaze->query("SELECT nazev_v_url FROM ad_rubriky WHERE id = ?",$idRubriky);
      $nazevVURL = $dotaz->fetch();
      $nazevVURL = $nazevVURL[0];
      
      $dotaz = $this->databaze->query("SELECT id_nadrazene_rubriky FROM ad_rubriky WHERE id = ?",$idRubriky);
      $idecko = $dotaz->fetch();
      $hodnotaKVraceni = false;
      
      $urlRubriky = new URLRubriky();
      
      $regVyraz = "";
      $cimNahradit = "";
      if ($idecko[0] == 0) {
         $regVyraz = "#^(".$nazevVURL."/)(.*)$#";
         $cimNahradit = "$2";
      }
      else {
         $nazevVURL = preg_replace("#^(.*/)([^/]+)$#", "$2",$nazevVURL);
         $regVyraz = "#^(.*/)(".$nazevVURL."/)(.*)$#";
         $cimNahradit = "$1$3";
      }
      
      $urlRubriky->ZmenUrlRubriky($this->databaze, $idRubriky, $regVyraz, $cimNahradit);
      
      $this->databaze->exec("UPDATE ad_rubriky SET id_nadrazene_rubriky = ? WHERE id_nadrazene_rubriky = ?",$idecko[0],$idRubriky);
      
      if ($trueOrFalseOridNoveRubriky === false) {
         if ($this->databaze->exec("DELETE FROM ad_rubriky WHERE id = ?",$idRubriky) == 1) {
         $this->databaze->exec("DELETE FROM ad_hlavni_menu WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $idRubriky);
         $hodnotaKVraceni = true;       
         }
         else $hodnotaKVraceni = false;
      }
      else if ($trueOrFalseOridNoveRubriky === true) {
         if ($this->databaze->exec("DELETE FROM ad_rubriky WHERE id = ?",$idRubriky) == 1) {
            if ($this->databaze->exec("DELETE FROM ad_stranky WHERE id_nadrazene_rubriky = ?",$idRubriky) != 0) {
               $this->databaze->exec("DELETE FROM ad_hlavni_menu WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $idRubriky);
               
               $malydotaz = $this->databaze->query("SELECT id FROM ad_stranky WHERE id_nadrazene_rubriky = ?", $idRubriky);
               while ($malyvysledek = $malydotaz->fetch(\PDO::FETCH_ASSOC)) {
                  $this->databaze->exec("DELETE FROM ad_hlavni_menu WHERE id_polozky = ? AND typ_polozky = 'stranka'", $malyvysledek["id"]);
               }
               
               $hodnotaKVraceni = true;         
            }
            else $hodnotaKVraceni = false;   
         }
         else $hodnotaKVraceni = false;
         
      }
      else {
         if ($this->databaze->exec("DELETE FROM ad_rubriky WHERE id = ?",$idRubriky) == 1) {
            $this->databaze->exec("DELETE FROM ad_hlavni_menu WHERE id_polozky = ? AND typ_polozky = 'rubrika'", $idRubriky);
            if ($this->databaze->exec("UPDATE ad_stranky SET id_nadrazene_rubriky = ? WHERE id_nadrazene_rubriky = ?",$trueOrFalseOridNoveRubriky,$idRubriky) != 0) {
               $hodnotaKVraceni = true;   
            }
            else $hodnotaKVraceni = false;   
         }
         else $hodnotaKVraceni = false;
      }
      
      return $hodnotaKVraceni;
   }
   

   /*------------------------------METODY V PROCESU STRÁNEK-------------------------------------*/

   public function VratStranky() {
      $dotaz = $this->databaze->query("SELECT id, nazev, id_nadrazene_rubriky, id_autora, datum_vytvoreni, datum_posledni_upravy FROM ad_stranky");
      $stranky = array();
      if ($dotaz) {
         while ($vysledek = $dotaz->fetch(\PDO::FETCH_OBJ)) {
         $stranka = new \StdClass;
         $stranka->id = $vysledek->id;
         $stranka->nazev = $vysledek->nazev;
        
         $dotaz1 = $this->databaze->query("SELECT nazev FROM ad_rubriky WHERE id = ?",$vysledek->id_nadrazene_rubriky);
         $vysledek1 = $dotaz1->fetch();

			$dotaz2 = $this->databaze->query("SELECT login, jmeno, prijmeni FROM ad_uzivatele WHERE id = ?", $vysledek->id_autora);	
			$radek2 = $dotaz2->fetch(\PDO::FETCH_ASSOC);
         $login_autora = "";
         if ($radek2["jmeno"] == "" && $radek2["prijmeni"] == "") {
            $login_autora  = $radek2["login"];
         }
         else {
            $login_autora  = $radek2["jmeno"] ." ". $radek2["prijmeni"];  
         }
         
         $stranka->nadrazenaRubrika = ($vysledek1[0] == null) ? "– – – – – " : $vysledek1[0];
         $stranka->jmenoAutora = $login_autora;
         $stranka->id_autora = $vysledek->id_autora;
         $stranka->pole_roli_autora = $this->VratRoleUzivatele($vysledek->id_autora);
         $stranka->datum_vytvoreni = $vysledek->datum_vytvoreni;
         $stranka->datum_posledni_upravy = $vysledek->datum_posledni_upravy;
         $stranky[] = $stranka;
         }
         return $stranky;
      }
      else {
         return false;
      }
   }
   
    public function VratStrankyProMenu() {
      $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_stranky");
      $stranky = array();
      $test = $dotaz->fetch();
      if ($test[0] != null) {
      $dotaz = $this->databaze->query("SELECT id, nazev, id_nadrazene_rubriky, nazev_v_url FROM ad_stranky ORDER BY id");
         while ($vysledek = $dotaz->fetch(\PDO::FETCH_OBJ)) {
         $stranka = new \StdClass;
         $stranka->id = $vysledek->id;
         $stranka->nazev = $vysledek->nazev;       
         $stranka->idNadrazeneRubriky = $vysledek->id_nadrazene_rubriky;
         $stranka->adresa = $vysledek->nazev_v_url;
         $stranky[] = $stranka;
         }
         return $stranky;
      }
      else {
         return false;
      }
   }
   
   
   public function VratDataProUpraveniStranky($idStranky) {
      $dotaz = $this->databaze->query("SELECT nazev,id_nadrazene_rubriky,obsah,nazev_v_url,id_autora FROM ad_stranky WHERE id = ?",$idStranky);
      $vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC);
      $stranka = new \StdClass;
      $stranka->nazev = $vysledek["nazev"];
      $stranka->id_nadrazene_rubriky = $vysledek["id_nadrazene_rubriky"];
      $stranka->obsah = $vysledek["obsah"];
      $stranka->nazev_v_url = $vysledek["nazev_v_url"];
      $stranka->id_autora = $vysledek["id_autora"];
      $stranka->role_autora = $this->VratRoleUzivatele($vysledek["id_autora"]);
      return $stranka;
   }
  
   
   public function VratStrankyRubriky($idRubriky) {
      $dotaz = $this->databaze->query("SELECT nazev FROM ad_stranky WHERE id_nadrazene_rubriky =?", $idRubriky);
      $stranky = "";
      while ($vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
         $stranky += $vysledek["nazev"] ."\n";
      }
      return $stranky;
   }  
   
   public function VytvoreniStranky($nazev, $nazevVUrl, $hiearchie, $obsahStranky, $userid) {
      if ($this->databaze->exec("INSERT INTO ad_stranky (nazev, id_nadrazene_rubriky, obsah, nazev_v_url, id_autora,datum_vytvoreni, datum_posledni_upravy) VALUES (?,?,?,?,?,?,?)", $nazev, $hiearchie, $obsahStranky, $nazevVUrl, $userid, new \DateTime, new \DateTime) == 1) {
         return true;
      }
      else return false;
   }
   
   public function UpraveniStranky($nazev, $nazevVUrl, $hiearchie, $obsahStranky, $userid, $idStranky) {
      $this->databaze->exec("UPDATE ad_stranky SET nazev = ?, id_nadrazene_rubriky = ?, obsah = ?, nazev_v_url = ?, id_autora = ?, datum_posledni_upravy = ? WHERE id = ?", $nazev, $hiearchie, $obsahStranky, $nazevVUrl, $userid, new \DateTime, $idStranky);
         $this->databaze->exec("UPDATE ad_hlavni_menu SET adresa = ? WHERE id_polozky = ? AND typ_polozky = 'stranka'", $nazevVUrl, $idStranky);
         $this->databaze->exec("UPDATE ad_hlavni_menu SET nazev = ? WHERE id_polozky = ? AND typ_polozky = 'stranka'", $nazev, $idStranky);
         return true;
   }
   
   public function SmazStranku($idStranky) {
      if ($this->databaze->exec("DELETE FROM ad_stranky WHERE id = ?", $idStranky) == 1) {
         $this->databaze->exec("DELETE FROM ad_hlavni_menu WHERE id_polozky = ? AND typ_polozky = 'stranka'", $idStranky);
         return true;
      }
      else return false;
   }
   
   /*------------------------------METODY V PROCESU MENU-------------------------------------*/	
   
   public function ExistujeMenu() {
      $dotaz = $this->databaze->query("SELECT COUNT(*) FROM ad_hlavni_menu");
      $vysledek = $dotaz->fetch();
      if ($vysledek[0]) {
         return true;
      }
      else return false;
   }
   
   public function VytvorMenu() {
      $dotaz = $this->databaze->query("SELECT id, nazev, id_nadrazene_rubriky, nazev_v_url FROM ad_rubriky");
      while ($vysledek = $dotaz->fetch(\PDO::FETCH_OBJ)) {
         if ($this->databaze->exec("INSERT INTO ad_hlavni_menu (id_polozky, id_nadrazene_polozky, nazev, typ_polozky, adresa ) VALUES (?,?,?,?,?)", $vysledek->id, $vysledek->id_nadrazene_rubriky, $vysledek->nazev, "rubrika", $vysledek->nazev_v_url) != 1) return false;
      }
      $poleId = array();
      $poleNadrazenePolozky = array();
      $dotaz = $this->databaze->query("SELECT id, id_polozky, id_nadrazene_polozky FROM ad_hlavni_menu ORDER BY id ASC");
      while ($vysledek = $dotaz->fetch(\PDO::FETCH_ASSOC)) {
         $poleId[$vysledek["id_polozky"]] = $vysledek["id"];
         $poleNadrazenePolozky[$vysledek["id"]] = $vysledek["id_nadrazene_polozky"];
      }
     // die($poleId[5]);
      foreach ($poleNadrazenePolozky as $klic => $hodnota) {
         if ($hodnota != 0) {
            $this->databaze->exec("UPDATE ad_hlavni_menu SET id_nadrazene_polozky = ? WHERE id = ?", $poleId[$hodnota], $klic);
         }
      }
      return true;
   }
   
   public function VratPolozkyHlavnihoMenu() {

      $poleSUdaji = array("id","id_polozky", "nazev","id_nadrazene_polozky", "typ_polozky","adresa");
      $treeMenu = new Tree($this->databaze,"id_nadrazene_polozky", "ad_hlavni_menu",0,$poleSUdaji);
      return $treeMenu;
   }
   
   public function UpravMenu($poradi, $id_polozky, $nadrazene_id, $nazev, $typ, $adresa) {
      $this->databaze->exec("TRUNCATE TABLE ad_hlavni_menu");
      $pocet = count($poradi);
      for ($i = 0;$i < $pocet;$i++) {
         $this->databaze->exec("INSERT INTO ad_hlavni_menu (id, id_polozky, id_nadrazene_polozky, nazev, typ_polozky, adresa) VALUES (?,?,?,?,?,?)", $poradi[$i], $id_polozky[$i], $nadrazene_id[$i], $nazev[$i], $typ[$i], $adresa[$i]);
      }
   }
   
   /*------------------------------METODY V PROCESU VYTVOŘENÍ NOVÉHO KVÍZU-------------------------------------*/	

	public function VytvorNovyKviz($idUzivatele, $nazevKvizu, $public, $otazky, $odpovedi, $typKvizu) {
		
		if ($public)
			$public = "ano";
		else
			$public = "ne";
		
		$pocetOtazek = count($otazky);	
		if ($typKvizu == "kvizBezMoznosti") {
		
			$this->databaze->exec("INSERT INTO az_quizs (id_az_user, name, public, date, have_options) VALUES (?, ?, ?, ?, 'ne')", $idUzivatele, $nazevKvizu, $public, new DateTime);

			$idKvizu = $this->databaze->lastInsertId();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				$this->databaze->exec("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES (?, ?, ?, ?)", $idKvizu,$idUzivatele,$aktualniOtazka, ($i+1));

				$idOtazky = $this->databaze->lastInsertId();

					$aktualniOdpoved = $odpovedi[$i];
					
					$this->databaze->exec("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES (?, ?, ?, ?, 'ano', '1')", $idKvizu, $idOtazky, $idUzivatele, $aktualniOdpoved);
			}
		
		}
		else {
			
			$this->databaze->exec("INSERT INTO az_quizs (id_az_user, name, public, date) VALUES (?, ?, ?, ?)", $idUzivatele, $nazevKvizu, $public, new DateTime);

			$idKvizu = $this->databaze->lastInsertId();
			
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				
				$this->databaze->exec("INSERT INTO az_questions (id_az_quiz, id_az_user, name, order_number) VALUES (?, ?, ?, ?)", $idKvizu,$idUzivatele,$aktualniOtazka, ($i+1));
				
				$idOtazky = $this->databaze->lastInsertId();
				
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					
					$this->databaze->exec("INSERT INTO az_answers (id_az_quiz, id_az_question, id_az_user, name, right_answer, order_number) VALUES (?, ?, ?, ?, ?, ?)", $idKvizu, $idOtazky, $idUzivatele, $aktualniOdpoved, $spravnaOdpoved, $poradiOdpovedi);
						
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
	}
/*------------------------------METODY V PROCESU ZOBRAZENÍ VLASTNÍCH KVÍZŮ-------------------------------------*/		
	
	public function ExistujiVytvoreneKvizy($idUzivatele) {
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE id_az_user = ?",$idUzivatele);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] >= 1)
			return true;
		else return false;
	}
	
	public function VratSeznamKvizu($idUzivatele) {
		$seznamKvizu = array();
		$i = 0;
		$dotaz = $this->databaze->query("SELECT id, name, date, have_options, public FROM az_quizs WHERE id_az_user = ? ORDER BY date DESC",$idUzivatele);

		while($radek = $dotaz->fetch(PDO::FETCH_ASSOC)) {
			$seznamKvizu[$i]["id"] = $radek["id"];
			$seznamKvizu[$i]["name"] = $radek["name"];
			$seznamKvizu[$i]["date"] = $radek["date"];
			$seznamKvizu[$i]["have_options"] = $radek["have_options"];
			$seznamKvizu[$i]["public"] = $radek["public"];
			$i += 1;
			//array (array("id" => , "name" =>, "date" => ), array("id" => , "name" =>, "date" => ))
		}		
		return $seznamKvizu;
	}
	
/*------------------------------METODY V PROCESU ÚPRAVY KVIZU-------------------------------------*/		
	
	public function SmazKviz($idUzivatele, $idKvizu){
		$pocetSmazanychZaznamu = $this->databaze->exec("DELETE FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);

		if ($pocetSmazanychZaznamu == 1) {
			$this->databaze->exec("DELETE FROM az_questions WHERE id_az_quiz = ?", $idKvizu);
			$this->databaze->exec("DELETE FROM az_answers WHERE id_az_quiz = ?", $idKvizu);
			return true;
		}
		else 
			return false;
	}
	
	public function UpravKviz($idUzivatele, $idKvizu, $nazevKvizu, $public, $otazky, $odpovedi) {

		$pocetOtazek = count($otazky);	
		
		$pocetOdpovedi = count($odpovedi);

		$dotaz = $this->databaze->exec("UPDATE az_quizs SET name = ?, public = ? WHERE id = ? AND id_az_user = ?", $nazevKvizu, $public, $idKvizu, $idUzivatele);
		
		if (!$this->ObsahujeKvizMoznosti($idKvizu)) {
		
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				
				$dotaz = $this->databaze->exec("UPDATE az_questions SET name = ? WHERE id_az_quiz = ? AND order_number = ?", $aktualniOtazka, $idKvizu, ($i+1));
				
				$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, ($i+1));
				$objektVysledku = $dotaz->fetch();			
				$idOtazky = $objektVysledku[0];
					
				$aktualniOdpoved = $odpovedi[$i];
				
				$this->databaze->exec("UPDATE az_answers SET name = ? WHERE id_az_quiz = ? AND id_az_question = ? AND order_number = '1'", $aktualniOdpoved, $idKvizu, $idOtazky);
						
			}
		}
		else {
			for ($i = 0; $i < $pocetOtazek; $i++) {
				$aktualniOtazka = $otazky[$i];
				$this->databaze->exec("UPDATE az_questions SET name = ? WHERE id_az_quiz = ? AND order_number = ?",$aktualniOtazka, $idKvizu, ($i+1));

				$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, ($i+1));
				$objektVysledku = $dotaz->fetch();			
				$idOtazky = $objektVysledku[0];
				$poradiOdpovedi = 1;
				
				for ($j = $i * 3; $j < ($i * 3) + 3; $j++) {
					$spravnaOdpoved = 'ne';
					
					if ($j % 3 == 0) {
						$spravnaOdpoved = 'ano';
					}
					//echo $right;
					$aktualniOdpoved = $odpovedi[$j];
					
					$this->databaze->exec("UPDATE az_answers SET name = ?, right_answer = ? WHERE id_az_quiz = ? AND id_az_question = ? AND order_number = ?",$aktualniOdpoved,$spravnaOdpoved,$idKvizu,$idOtazky,$poradiOdpovedi);
						
					$poradiOdpovedi += 1;
				}
			}
		}
		
	}
	
	public function ExistujeKvizProUzivatele($idUzivatele, $idKvizu) {
	
		$dotaz = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == 1)
			return true;
		else return false;
	
	} 
	
	public function VratVlastnostiKvizu($idKvizu){
		$dotaz = $this->databaze->query("SELECT name, public FROM az_quizs WHERE id = ?", $idKvizu);
		
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$vlastnostiKvizu["name"] = $radek->name;
			$vlastnostiKvizu["public"] = $radek->public;
		}	
		
		return $this->OdescapujData($vlastnostiKvizu);
		
	} 
	public function VratOtazkyKvizu($idKvizu) {
		$dotaz = $this->databaze->query("SELECT name FROM az_questions WHERE id_az_quiz = ? ORDER BY order_number ASC", $idKvizu);
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$otazkyKvizu[] = $radek->name;
		}	
		
		return $this->OdescapujData($otazkyKvizu);
	}
	
	public function VratOdpovediKvizu($idKvizu) {
		$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_quiz = ? ORDER BY id ASC", $idKvizu);
		while($radek = $dotaz->fetch(PDO::FETCH_OBJ)) {
			$odpovediKvizu[] = $radek->name;
		}	
		
		return $this->OdescapujData($odpovediKvizu);
	}
	
	public function ObsahujeKvizMoznosti($idKvizu) {
	
		$dotaz = $this->databaze->query("SELECT have_options FROM az_quizs WHERE id = ?", $idKvizu);
		$objektVysledku = $dotaz->fetch();
		if ($objektVysledku[0] == "ano") {
			return true;
		}
		else return false;
	}
/*------------------------------METODY V PROCESU Vyhledávání-------------------------------------*/		
	public function VratPocetVysledkuProDotaz($dotaz) {
		if ($dotaz == "*") { 
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE public = 'ano'");
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs");
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		else {
			$dotaz = '%'.$dotaz.'%';
			$pocetVysledku = $this->databaze->query("SELECT COUNT(*) FROM az_quizs WHERE public = 'ano' AND name LIKE ?",$dotaz);
			$pocetVysledku = $pocetVysledku->fetch();
			$pocetVysledku = $pocetVysledku[0];
		}
		return $pocetVysledku;
	}
	
	public function VratSeznamKvizuProDotaz($dotaz) {
		$seznamKvizu = array();
		$i = 0;
		if ($dotaz == "*") {
			$query = $this->databaze->query("SELECT * FROM az_quizs WHERE public = 'ano'");
		}
		else if ($dotaz == "*herech@seznam.cz*") {
			$query = $this->databaze->query("SELECT * FROM az_quizs");
		}
		else {
			$dotaz = '%'.$dotaz.'%';
			$query = $this->databaze->query("SELECT * FROM az_quizs WHERE public = 'ano' AND name LIKE ?", $dotaz);
		}
		while ($radek = $query->fetch(PDO::FETCH_OBJ)) {
			$seznamKvizu[$i]["id"] = $radek->id;
			$seznamKvizu[$i]["name"] = $radek->name;
			$dotaz1 = $this->databaze->query("SELECT login FROM az_users WHERE id = ?", $radek->id_az_user);
			$objektVysledku = $dotaz1->fetch();
			$login = $objektVysledku[0];
			$seznamKvizu[$i]["login"] = $login;
			$i += 1;
		}
		return $seznamKvizu;
	}
/*------------------------------METODY V PROCESU SAMOTNÉ HRY AZ KVÍZ-------------------------------------*/		
	
	public function JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu) {
		$dotaz = $this->databaze->query("SELECT * FROM az_quizs WHERE id = ? AND id_az_user = ?", $idKvizu, $idUzivatele);
		if ($dotaz->columnCount() >= 1) 
			return true;
		else {
		$dotaz = $this->databaze->query("SELECT * FROM az_quizs WHERE id = ? AND public = 'ano'", $idKvizu);
		if ($dotaz->columnCount() >= 1) 
			return true;
		else 
			return false;
		}
	}
	
	public function VratDataDoOknaKvizu($idUzivatele, $idKvizu, $cisloOtazky) {
		if ($this->JeKvizVerejnyProSoucasnehoUzivatele($idUzivatele, $idKvizu)) {
			$dotaz = $this->databaze->query("SELECT name FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, $cisloOtazky);
			$objektVysledku = $dotaz->fetch();
			$vracenaData["otazka"] = $objektVysledku[0];
			
			$dotaz = $this->databaze->query("SELECT id FROM az_questions WHERE id_az_quiz = ? AND order_number = ?", $idKvizu, $cisloOtazky);
			$objektVysledku = $dotaz->fetch();
			$idOtazky = $objektVysledku[0];
			
			if ($this->ObsahujeKvizMoznosti($idKvizu)) {
			
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '1'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved1"] = $objektVysledku[0];
				
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '2'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved2"] = $objektVysledku[0];
				
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '3'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved3"] = $objektVysledku[0];
			}
			else {
				$dotaz = $this->databaze->query("SELECT name FROM az_answers WHERE id_az_question = ? AND order_number = '1'", $idOtazky);
				$objektVysledku = $dotaz->fetch();
				$vracenaData["odpoved1"] = $objektVysledku[0];
			}
			
			$vracenaData = $this->OdescapujData($vracenaData);
			
			return $vracenaData;
		}
		else return false;
	}
	
	public function OdescapujData($data) {
		$odescapovanaData = array();
		if (is_array($data)) {
		 foreach ($data as $index => $prvek) {
			$odescapovanaData[$index] = preg_replace("/\\\\+'/","'",$prvek);
		 }
		}
		else {
			$odescapovanaData = preg_replace("/\\\\+'/","'",$data);
		}
		return $odescapovanaData;
	}
}