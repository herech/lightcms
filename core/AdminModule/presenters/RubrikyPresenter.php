<?php
namespace AdminModule; 
use Nette\Application\UI\Form;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Nette\Application\Responses\JsonResponse;

class RubrikyPresenter extends BasePresenter
{
   public $obsahRubriky = "";
   
   public function handleNactiUrl() {
      $databaze = new Databaze();
      $url = $databaze->NactiUrlRubriky($_GET["id"]);
      $this->sendResponse(new JsonResponse(array("url" => $url)));
   }
   public function handleVratRubriky() {
      $this['vypisStromu'] = new VypisStromu();
		$databaze = new Databaze();
		$rubriky = $databaze->VratRubriky();
      $rubrikyNabidka = $this['vypisStromu']->VypisProOdstraneniRubriky($rubriky, $_GET["id"]);
      $this->sendResponse(new JsonResponse(array("rubrikyNabidka" => $rubrikyNabidka)));
   }

   public function actionOdstraneniRubriky($idRubriky) {
      $databaze = new Databaze();
      $user = $this->getUser();   
      $rubrika = $databaze->DataPristupnostiRubriky($idRubriky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $rubrika->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $rubrika->id_autora)) {
         $databaze->SmazRubriku($idRubriky, false);
         $this->flashMessage("Rubrika byla úspěšně smazána","success");
      }
      else {
         $this->flashMessage("Nemáte oprávnění mazat danou rubriku","error");
      }
      $this->redirect("Rubriky:");
   }
   
   
	public function renderDefault()
	{

         $this['vypisStromu'] = new VypisStromu();
         $databaze = new Databaze();
         $rubriky = $databaze->VratRubriky();
         //die(print_r($rubriky));
         if ($rubriky == false) {
            $this->template->rubriky = false;
         }
         else {
            $this->template->rubriky = $this['vypisStromu']->VypisProRubriky($rubriky);
         }
     
	}
   
   
   public function renderVytvoreniRubriky() {
      $user = $this->getUser();   
      if ($user->isInRole("Redaktor") && !$user->isInRole("Šéfredaktor")) { 
         $this->flashMessage("Nemáte oprávnění tvořit rubriky","error");
         $this->redirect("Rubriky:");
      }
      else {
         $this['vypisStromu'] = new VypisStromu();
         $databaze = new Databaze();
         $rubriky = $databaze->VratRubriky();
         if ($rubriky == false) {
            $this->template->rubriky = false;
         }
         else {
            $rubriky = $this['vypisStromu']->VypisProVytvoreniRubriky($rubriky);
            $this->template->rubriky = $rubriky ;
         }
      }
   }
   
   protected function createComponentVytvoreniRubrikyForm()
	{
		$form = new Form();
		$form->addProtection();
		$form->addText('nazev', 'Název*: ')
		->setRequired("Musíte Vyplnit název rubriky");
		$form->addText('nazevVUrl', 'Název v URL: ');		
      $form->addSelect('hiearchie','Hiearchie*: ');
      $form->addRadioList('typObsahuRubriky', 'Obsah rubriky: ', array("vychozi" => " Výchozí | ", "vlastni" => " Vlastní"))
      ->getSeparatorPrototype()->setName(NULL);
      $form['typObsahuRubriky']->setDefaultValue('vychozi');
		$form->addTextArea('obsahRubriky','');
		$form->addSubmit('vytvoritRubriku', 'Vytvořit novou rubriku');
		$form->onSuccess[] = $this->vytvoreniRubrikyFormSubmitted;
		return $form;
	}

	public function vytvoreniRubrikyFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();
         if ($user->isInRole("Redaktor") && !$user->isInRole("Šéfredaktor")) {
            $this->flashMessage('Rubrika nebyla vytvořena, protože nemáte oprávnění ji tvořit.', 'error');
            $this->redirect('this');
         }
         else {
            $nazevVUrl = ($values->nazevVUrl == "") ? Strings::webalize($values->nazev) : $values->nazevVUrl;
            if ($databaze->jeUrlObsazene($nazevVUrl)) {
               $form->addError("Dané URL je již obsazené, zvolte prosím jiné");
            }
            else {
               $obsahRubriky = ($values->typObsahuRubriky == "vychozi") ? "" : $values->obsahRubriky;
               
               if ($databaze->VytvoreniRubriky($values->nazev, $_POST['hiearchie'], $obsahRubriky, $nazevVUrl, $user->id)) {
                  $this->flashMessage('Rubrika byla vytvořena', 'success'); 
               } 
               else {
                  $this->flashMessage('Vyskytla se chyba, rubrika nemohla být uložena.', 'error');
               }
               $this->redirect('this');
            }
         }
	}
   
   
   public function renderUpraveniRubriky($idRubriky) {
      $databaze = new Databaze();   
      $user = $this->getUser();      
      $rubrika = $databaze->VratDataProUpraveniRubriky($idRubriky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $rubrika["role_autora"])) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $rubrika["id_autora"])) {

         $this['vypisStromu'] = new VypisStromu();
         $databaze = new Databaze();
         $rubriky = $databaze->VratRubriky();
         if ($rubriky == false) {
            $this->template->rubriky = false;
         }
         else {
            $rubriky = $this['vypisStromu']->VypisProUpravuRubriky($rubriky, $rubrika["id_nadrazene_rubriky"]);
            $this->template->rubriky = $rubriky ;
         }
      
         $this->obsahRubriky = $rubrika["obsah"];
         $this->template->nazev = $rubrika["nazev"];
         $this->template->nazev_v_url = $rubrika["nazev_v_url"];
         $this->template->obsah = $rubrika["obsah"];
         $this->template->idRubriky = $idRubriky;
      }
      else {
         $this->redirect('Rubriky:');
      }
   }

   protected function createComponentUpraveniRubrikyForm()
	{  
   
		$form = new Form();
		$form->addProtection();
		$form->addText('nazev', 'Název*: ')
		->setRequired("Musíte Vyplnit název rubriky");
		$form->addText('nazevVUrl', 'Název v URL: ');		
      $form->addSelect('hiearchie','Hiearchie*: ');
      $form->addRadioList('typObsahuRubriky', 'Obsah rubriky: ', array("vychozi" => " Výchozí | ", "vlastni" => " Vlastní"))
      ->getSeparatorPrototype()->setName(NULL);
      if ($this->obsahRubriky == "") {
         $form['typObsahuRubriky']->setDefaultValue('vychozi');
      }
      else {
         $form['typObsahuRubriky']->setDefaultValue('vlastni');
      }
		$form->addTextArea('obsahRubriky','');
      $form->addHidden('idRubriky');
		$form->addSubmit('upravitRubriku', 'Upravit rubriku');
		$form->onSuccess[] = $this->upraveniRubrikyFormSubmitted;
		return $form;
	}

	public function upraveniRubrikyFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();
      $rubrika = $databaze->DataPristupnostiRubriky($values->idRubriky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $rubrika->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $rubrika->id_autora)) {
         $nazevVUrl = ($values->nazevVUrl == "") ? Strings::webalize($values->nazev) : $values->nazevVUrl;
         $obsahRubriky = ($values->typObsahuRubriky == "vychozi") ? "" : $values->obsahRubriky;
         
         if ($databaze->jeUrlObsazene($nazevVUrl, $values->idRubriky, "rubrika")) {
            $form->addError("Dané URL je již obsazené, zvolte prosím jiné");   
         }
         else {
            if ($databaze->UpraveniRubriky($values->nazev, $_POST['hiearchie'], $obsahRubriky, $nazevVUrl, $user->id, $values->idRubriky)) {
               $this->flashMessage('Rubrika byla upravena', 'success'); 
            } 
            else {
               $this->flashMessage('Vyskytla se chyba, rubrika nemohla být upravena.', 'error');
            }
         }
		}
		else {
         $this->flashMessage('Rubrika nebyla upravena, protože nemáte oprávnění ji upravovat.', 'error');
		}
      $this->redirect('this');
	}
   
   protected function createComponentSmazaniRubrikyForm()
	{  
      $databaze = new Databaze();
      //$poleRubrik = $databaze->VratPoleRubrik();
		$form = new Form();
		$form->addProtection();
		$form->addRadioList('akce', 'Akce ', array("smazat" => " Smazat stránky rubriky","priradit" => " Přiřadit stránky nové rubrice: "));
      $form['akce']->setDefaultValue('smazat');
		$form->addSelect("rubriky", "rubriky");//, $poleRubrik);
      //$form['rubriky']->setDefaultValue(0);
      $form->addHidden('idRubriky');
		$form->addSubmit('smazatRubriku', 'Smazat rubriku');
		$form->onSuccess[] = $this->smazaniRubrikyFormSubmitted;
		return $form;
	}
   
   public function smazaniRubrikyFormSubmitted(Form $form)
	{
		$values = $form->getValues();
		$databaze = new Databaze();
		$user = $this->getUser();
      $rubrika = $databaze->DataPristupnostiRubriky($values->idRubriky);
      if (($user->isInRole("Administrátor") && !in_array("Administrátor", $rubrika->role_autora)) || ($user->isInRole("Hlavní Administrátor")) || ($user->id == $rubrika->id_autora)) {
         //die($values->akce);
         if ($values->akce == "smazat") {
            if ($databaze->SmazRubriku($values->idRubriky, true)) {
               $this->flashMessage('Rubrika a její stránky byly smazány.', 'success');
            }
            else $this->flashMessage('Vyskytla se chyba. Rubrika a její stránky nemohli být smazány.', 'error');
         }
         else if ($values->akce == "priradit") {
            $rubrika = $databaze->VratDataProUpraveniRubriky($values->rubriky);
            $databaze->SmazRubriku($values->idRubriky, $values->rubriky);
            $this->flashMessage('Stránky mazené rubriky byly přiřazeny nové rubrice: '. $rubrika["nazev"] .'','success');
            
         }
		}
		else {
         $this->flashMessage('Nemáte oprávnění mazat rubriky.', 'error');
		}
      $this->redirect('this');
	}
   

}
/*AdminModule\Tree Object ( 
   [poleSKonkretnimiUdaji] => Array ( ) 
   [level] => -1 
   [children] => Array ( 
      [0] => AdminModule\Tree Object ( 
         [poleSKonkretnimiUdaji] => Array ( 
            [id] => 1 
            [nazev] => jedna 
            [id_nadrazene_rubriky] => 0 
            [obsah] => 
            [nazev_v_url] => jedna 
            [id_autora] => 0 
            [role_autora] => Array ( [0] => ) 
         ) 
         [level] => 0 
         [children] => Array (
            [0] => AdminModule\Tree Object ( 
               [poleSKonkretnimiUdaji] => Array ( 
                  [id] => 3 
                  [nazev] => dva 
                  [id_nadrazene_rubriky] => 1 
                  [obsah] => 
                  [nazev_v_url] => dva 
                  [id_autora] => 0 
                  [role_autora] => Array ( [0] => ) 
               ) 
               [level] => 1 [children] => Array ( ) 
            ) 
            [1] => AdminModule\Tree Object ( 
               [poleSKonkretnimiUdaji] => Array ( 
                  [id] => 4 
                  [nazev] => dva01 
                  [id_nadrazene_rubriky] => 1 
                  [obsah] => 
                  [nazev_v_url] => dva01 
                  [id_autora] => 0 
                  [role_autora] => Array ( [0] => ) 
               ) 
               [level] => 1 
               [children] => Array ( 
                  [0] => AdminModule\Tree Object ( 
                     [poleSKonkretnimiUdaji] => Array ( 
                        [id] => 5 
                        [nazev] => tri 
                        [id_nadrazene_rubriky] => 4 
                        [obsah] => 
                        [nazev_v_url] => tri 
                        [id_autora] => 0 
                        [role_autora] => Array ( [0] => ) 
                     ) 
                     [level] => 2 
                     [children] => Array ( ) 
                  ) 
                  [1] => AdminModule\Tree Object ( 
                     [poleSKonkretnimiUdaji] => Array ( 
                        [id] => 6 
                        [nazev] => tri01 
                        [id_nadrazene_rubriky] => 4 
                        [obsah] => 
                        [nazev_v_url] => tri01 
                        [id_autora] => 0 
                        [role_autora] => Array ( [0] => ) 
                     ) 
                     [level] => 2 
                     [children] => Array ( ) 
                  ) 
               ) 
            ) 
         ) 
      ) 
      [1] => AdminModule\Tree Object ( 
         [poleSKonkretnimiUdaji] => Array (
            [id] => 2 
            [nazev] => jedna01 
            [id_nadrazene_rubriky] => 0 
            [obsah] => 
            [nazev_v_url] => jedna01 
            [id_autora] => 0 
            [role_autora] => Array ( [0] => ) 
         ) 
         [level] => 0 
         [children] => Array ( ) 
      ) 
   ) 
) 1 */
