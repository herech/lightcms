<?php

/**
 * Base presenter for all application presenters.
 */

namespace AdminModule; 
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Http\Url;
 
abstract class BasePresenter extends \Nette\Application\UI\Presenter
{

      protected function createTemplate($class = NULL)
      {
          $template = parent::createTemplate($class);
          $template->registerHelper('jePrvekVPoli', function ($prvek, $pole) {
              return in_array($prvek, $pole); 
          });
          return $template;
      }

	public function handleSignOut()
	{
		$this->getUser()->logout();
		$this->redirect('Homepage:');
	}

	protected function createComponentRegistrationForm()
	{
		$form = new Form();
		//$form->addProtection();
		$form->addText('email', 'Email: ')
		->setRequired('Zvolte si Email')
		->addRule(Form::EMAIL,'Zadejte prosím platný email')
		->setAttribute('type','email');
		$form->addPassword('password', 'Heslo:')
		->setRequired('Zvolte si heslo')
		->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 5)
		->setAttribute('autocomplete','off');
		$form->addPassword('password1', 'Heslo pro kontrolu:')
		->setRequired('Zadejte prosím heslo ještě jednou pro kontrolu')
		->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password'])
		->setAttribute('autocomplete','off');
		$form->addSubmit('registrace', 'Registrovat se');
		$form->onSuccess[] = $this->registrationFormSubmitted;
		return $form;
	}

	public function registrationFormSubmitted(Form $form)
	{
			$values = $form->getValues();
			$databaze = new Databaze();
			if ($databaze->OverJestliUzMameHlavnihoAdmina() == false) {
				$databaze->RegistrovatUzivatele($values->email, $values->password, $values->email);
				$this->flashMessage('Registrace proběhla úspěšně.', 'success');
				$httpRequest = $this->context->httpRequest;
				$url = new Url($httpRequest->getUrl());
				$uri = $url->authority;
				
				
				$urlPath = "/";
                if (preg_match("#^/www/.+$#",$url->path)) {
                     $urlPath = "/www/";
                }
				$uri2 = $url->hostUrl . $urlPath;
				$url = substr($uri, 4);
				
            /*--------------*/
            $adresa = $this->link("Homepage:default");
            $adresa = $_SERVER['SERVER_NAME'] . $adresa;
            /*--------------*/
				
				$formatEmailu = "<administrace@". $url .">";
				$mail = new Message;
				$mail->setFrom("Administrace $formatEmailu")
				->addTo($values->email)
				->setSubject('Nová registrace')
				->setBody("Dobrý den,\nbyl jste registrován jako Hlavní administrátor serveru $uri. Pro vstup do administrace následujte prosím tento odkaz: " . $adresa)
				->send();
				$this->redirect('Homepage:');
			}
	}
	
	protected function createComponentSignInForm()
	{
		$form = new Form();
		//$form->addProtection();
		$form->addText('username', 'Přihlašovací jméno:')
		->setRequired('Zadejte vaše přihlašovací jméno')
		->addRule(Form::EMAIL,'Zadejte prosím platný email')
		->setAttribute('type','email');
		$form->addPassword('password', 'Heslo:')
		->setRequired('Zvolte si heslo')
		->setAttribute('autocomplete','off');
		$form->addCheckbox('persistent', 'Pamatovat si mě na tomto počítači');
		$form->addSubmit('login', 'Přihlásit se');
		$form->onSuccess[] = $this->signInFormSubmitted;
		return $form;
	}

	public function signInFormSubmitted(Form $form)
	{
		try {
			$user = $this->getUser();
			$values = $form->getValues();
			if ($values->persistent) {
				$user->setExpiration('+30 days', FALSE);
			}
			else {
				$user->setExpiration(0, TRUE);
			}
			$user->login($values->username, $values->password);
			$this->flashMessage('Přihlášení bylo úspěšné.', 'success');
			$this->redirect('this');
		} catch (\Nette\Security\AuthenticationException $e) {
			$form->addError('Neplatné uživatelské jméno nebo heslo.');
		}
	}
	
	protected function createComponentNewPasswordForm() {
		$form = new Form();
		//$form->addProtection('Vypršel časový limit, odešlete formulář znovu');
		$form->addText("email", "Váš email:")
				->addRule(Form::EMAIL, "Zadejte platný email!")
				->setRequired("Zadejte platný email")
				->setAttribute("type", "email");
		$form->addPassword("password", "Vaše nové heslo: ")
				->setAttribute("autocomplete", "off")
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaky', 3)
				->setRequired("Zadejte heslo!");
		$form->addSubmit("submit", "Odeslat");
		$form->onSuccess[] = callback($this, "newPasswordFormSubmitted");
		return $form;
	}
	
	public function newPasswordFormSubmitted(Form $form) {
		
			$token = md5(uniqId(mt_rand(), true));
			$email = $form->values->email;
			$password = $form->values->password;
			$adresa = $this->link("ConfirmNewPassword:confirm", array("email" => $email, "password_token" => $token, "password" => hash('sha256', $password)));
			$adresa = $_SERVER['SERVER_NAME'] . $adresa;
			$uzivatel = new Uzivatel();
			if ($uzivatel->NastavNoveHeslo($email, $password, $token, rawurldecode($adresa))) 
				$this->flashMessage("Na zadaný email byl odeslán odkaz pro potvrzení nového hesla!", 'success');
			else
				$this->flashMessage("Vaše žádost nebyla zpracována, protože uživatel se zadaným emailem neexistuje!", 'error');
			$this->redirect('this');
		
	}
	
	public function beforeRender() {
      
		$databaze = new Databaze();
		$Uzivatel = $databaze->VratUdajeUzivatele($this->getUser()->id);
		
		
		if ($databaze->OverJestliUzMameHlavnihoAdmina() == false) {
			$this->template->registraceJizProbehla = false;
		}
		else {
			$this->template->registraceJizProbehla = true;
			$this->template->Uzivatel = $Uzivatel;
			$httpRequest = $this->context->httpRequest;
			$uri = $httpRequest->getUrl();
			$url = new Url($uri);
			$this->template->urlStranky = $url->authority;
		}
	}

	public function renderDefault() {
	
	}
}
