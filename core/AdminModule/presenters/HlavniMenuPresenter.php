<?php
namespace AdminModule; 

class HlavniMenuPresenter extends BasePresenter
{

	public function actionVytvorMenu() {
      $user = $this->getUser();
      if ($user->isInRole("Šéfredaktor")) {
         $databaze = new Databaze();
         if (!$databaze->VytvorMenu()) {
            $this->flashMessage("Menu nemohlo být vytvořeno. Vytvořte nejdříve alepoň jednu rubriku.","error");
         }
         else {
            $this->flashMessage("Menu bylo vytvořeno","success");
         }
         $this->redirect("HlavniMenu:");
      }
      else {
         $this->flashMessage("Nemáte oprávnění tvořit menu","error");
         $this->redirect("HlavniMenu:");
      }
   }

   public function actionUlozitMenu($poradi, $id_polozky, $nadrazene_id, $nazev, $typ, $adresa) {
     $user = $this->getUser();
      if ($user->isInRole("Šéfredaktor")) {
         $databaze = new Databaze();
         $poradi = explode(",", $poradi);
         $id_polozky = explode(",", $id_polozky);
         $nadrazene_id = explode(",", $nadrazene_id);
         $nazev = explode(",", $nazev);
         $adresa = explode(",", $adresa);
         $typ = explode(",", $typ);
         //die(var_dump($nadrazene_id));
         //die($nazev[count($nazev) - 1]);
         $databaze->UpravMenu($poradi, $id_polozky, $nadrazene_id, $nazev, $typ, $adresa);
         
         $this->flashMessage("Menu bylo upraveno","success");
         $this->redirect("HlavniMenu:");
      }
      else {
         $this->flashMessage("Nemáte oprávnění měnit menu","error");
         $this->redirect("HlavniMenu:");
      }
   }
   
   public function renderDefault()
	{

      $databaze = new Databaze();
      $pokracuj = true;
		if ($databaze->ExistujeMenu()) {
         $this->template->existujeMenu = true;
      }
      else {
         if ($databaze->ExistujiRubriky()) {
            $this->template->existujeMenu = false;
            $this->template->existujiRubriky = true;
         }
         else {
            $this->template->existujeMenu = false;
            $this->template->existujiRubriky = false;
            $pokracuj = false;
         }
      }
      if ($pokracuj) {
         $this['vypisStromu'] = new VypisStromu();
         $polozky = $databaze->VratPolozkyHlavnihoMenu();
         $polozky = $this['vypisStromu']->VypisProMenu($polozky);
         $this->template->polozky = $polozky;

         $this['vypisStromu']->retezec = "";
         $rubriky = $databaze->VratRubriky();
         $rubriky = $this['vypisStromu']->VypisProPridaniRubrikDoMenu($rubriky);
         $this->template->seznamRubrik = $rubriky;
         
         
         $stranky = $databaze->VratStrankyProMenu();
         if ($stranky) {
            $this->template->seznamStranek = $stranky;
         }
         else {
            $this->template->seznamStranek = false;
         }
      }
	}

}
